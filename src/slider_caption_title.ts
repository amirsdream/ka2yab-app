

export class SliderCaptionTitle{
    id:number = 0;
    type_animation:string = "skewfromright";
    data_x:string = "670";
    data_y:string = "290";
    data_start:string = "800";
    data_speed:string = "2000";
    data_easing:string = "Back.easeOut";
    data_endspeed:string = "500";
    z_index:number = 0;
    font_size:string = "40";
    style:string = "slide4-text3";
    title:string  = "";
    
}