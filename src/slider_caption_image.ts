

export class SliderCaptionImage{
    id:number = 0;
    type_animation:string = "skewfromright";
    image_name:string = "";
    data_x:string = "670";
    data_y:string = "290";
    data_start:string = "800";
    data_speed:string = "2000";
    data_easing:string = "Back.easeOut";
    data_endspeed:string = "500";
    z_index:number = 0;
    imageFile:  Array<File> = [] ;
    
}