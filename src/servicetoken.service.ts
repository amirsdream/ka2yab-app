import { Injectable }    from '@angular/core';
import { Headers, Http ,RequestOptions} from '@angular/http';
import { Storage } from '@ionic/storage';
import 'rxjs/Rx';
import {Observable} from "rxjs";
import 'rxjs/add/operator/toPromise';
// import {Category} from './category'

import 'rxjs/add/operator/toPromise';
import {Category1} from './category';

@Injectable()
export class BrandServiceToken {
    private headers = new Headers({'Content-Type': 'application/json'});

    private option = new RequestOptions({headers:this.headers});

    page: string = "admin";
    token: string = sessionStorage.getItem('token');
    constructor(private http: Http ,private storage:Storage) {
      this.storage.get('token').then((token)=>{
        this.token = token;
      })

      console.log("token -----------------")
      console.log(sessionStorage.getItem('token'));
    }


    create(url:string,formData:FormData): Promise<any> {
        formData.append('token',sessionStorage.getItem('token'));

        return this.http
            .post(url, formData)
            .toPromise()
            .then(res => res.json() )
            .catch(this.handleError);
    }

    get(url:string): Promise<any> {
        url=url+"/"+sessionStorage.getItem('token');
        return this.http.get(url)
            .toPromise()
            .then(response => response.json() )
            .catch((error)=>{
                console.error('error ', error)
                if(error.status == 0){
                    setTimeout(()=>{
                        // this.get(url);

                        this.get(url);
                        // alert(error.status);
                    },200);
                    // alert("error 500");
                }
                else{
                     return Promise.reject(error.message || error);
                }
            });
    }

    getOne(url:string,id:number): Promise<any> {
        const urlGet = `${url}/${id}/${sessionStorage.getItem('token')}`;
        return this.http.get(urlGet)
            .toPromise()
            .then(response => response.json() )
            .catch((error)=>{
                console.error('error ', error)
                if(error.status == 0){
                    setTimeout(()=>{
                        // this.get(url);

                        this.getOne(url,id);
                        // alert(error.status);
                    },200);
                    // alert("error 500");
                }
                else{
                     return Promise.reject(error.message || error);
                }
            });
    }
    getOneWithSlug(url:string,slug:string): Promise<any> {
        const urlGet = `${url}/${slug}/${sessionStorage.getItem('token')}`;
        return this.http.get(urlGet)
            .toPromise()
            .then(response => response.json() )
            .catch(this.handleError);
    }


    delete(url:string,id: number): Promise<void> {
      const urlDelete = `${url}/${id}/${sessionStorage.getItem('token')}`;
      return this.http.delete(urlDelete, {headers: this.headers})
      .toPromise()
      .then((res) => res.json() )
      .catch(this.handleError);
    }
    update(url:string,formData:FormData): Promise<any> {
        // const url = `${this.url}/${id}`;
        // const body = JSON.stringify({name:name,parent_id:parent_id});
        formData.append('token',sessionStorage.getItem('token'));
        const headers = new Headers ({'Content-Type':'application/json'});
        return this.http
        .put(url, formData,{headers:headers})
        .toPromise()
        .then((res) => res.json())
        .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }

    // ---------------------------------------------------------------------
  createObservable(url: string, formData: FormData,token) {
    formData.append('token', token);

    console.log("token______________________________")
    console.log(this.token)
    return this.http.post(url, formData).map((response) => {
      return response.json();
    });

  }

  getObservable(url: string,token): Observable<any> {
    url = url + '/' + token;
    return this.http.get(url).map(( response ) => {
        return response.json();
      }
    );
  }
  getOneObservable(url: string, id: number,token): Observable<any> {

    const urlGet = `${url}/${id}/${token}`;
    return this.http.get(urlGet).map(( response ) => {
        return response.json();
      }
    );
  }
  getOneWithSlugObservable(url: string, slug: string,token): Observable<any> {
    const urlGet = `${url}/${slug}/${token}`;
    return this.http.get(urlGet).map(( response ) => {
        return response.json();
      }
    );
  }
  deleteObservable(url: string, id: number,token) {
    const urlDelete = `${url}/${id}/${token}`;
    return this.http.delete(urlDelete)
      .map(
        (response) => {
          return response.json();
        });
  }
}
//   const body = JSON.stringify({content:newContent});
//         const headers = new Headers ({'Content-Type':'application/json'});
//         return this.http.put('http://127.0.0.1:8000/api/quote/'+id,body,{headers:headers})
