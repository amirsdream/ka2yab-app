import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {baseUrl} from '../../baseurl';
import{BrandService} from '../../brand.service';
import {User} from '../../user';
import {AuthenticatinService} from '../../authentication.service';
import { Storage } from '@ionic/storage';
import { LoadingController } from 'ionic-angular';

/**
 * Generated class for the Signin page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-signin',
  templateUrl: 'signin.html',
})
export class SigninPage {


  error = "";
  urlLogin :string=baseUrl+'api/users/login';
  loading = false ;

  constructor(public navCtrl: NavController, public navParams: NavParams,private brandService:BrandService,  private storage:Storage,
              private authService: AuthenticatinService,public loadingCtrl: LoadingController) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad SigninPage');
  }

  login() {
    this.navCtrl.setRoot('TabsPage');
  }

  register() {
    this.navCtrl.setRoot('SignupPage');
  }
  skip() {
    this.navCtrl.setRoot('TabsPage');
  }

  upload(email,pass){
    const loading = this.loadingCtrl.create({
      content: `
      <div class="custom-spinner-container  ">
        <div class="custom-spinner-box  "></div>
      </div>`,
    });

    this.error = "";
    const formData: any = new FormData();

    if(email == ""){
      this.error  +=  "فیلد ایمیل خالی می باشد.";
    }
    if( pass == ""){
      this.error  +=  "فیلد پسورد خالی می باشد.";
    }

    if(this.error == ""){
      formData.append("email",email);
      formData.append("pass",pass);
      this.loading = true ;
      loading.present();
      this.brandService.createObservable(this.urlLogin,formData).subscribe(
        res=>{
          // console.log(res.result);
          // console.log( res.result);
          this.loading = false ;
          loading.dismiss();
          if(res.result == true ){
            if(res.user.active == "true"){
              console.log("user---------------------------");
              console.log(res.user);
              console.log(res.token);
              this.storage.set('user',JSON.stringify(res.user)).then((val) => {
                this.storage.set('token',res.token).then((val) => {
                  console.log("token login----------------------------------")
                  console.log(val)
                  this.storage.get('user').then(value => {
                    console.log(value);
                    console.log('user login')
                    this.authService.emit(true);
                    this.navCtrl.setRoot('TabsPage');
                  });
                });
              });
              // this.storage.set('token',JSON.stringify(res.token));
              // sessionStorage['token'] = res.token;

              // console.log(sessionStorage.getItem('token'))


              // this.router.navigate(['/home']);
              // // console.log(sessionStorage['user']);
              // this.authService.isLogin();
            }
            else{
             this.error+="نام کاربری شما فعال نمی باشد."  ;
            }
          }
          else{
            this.error += "نام کاربری یا رمز عبور اشتباه است.";
          }
          // alert("عملیات شما با موفقیت انجام شد");
          // this.router.navigate(['/dashboard/user/show']);
        });
    }
    else{
      //alert(this.error);
      this.error="";
    }
  }
}
