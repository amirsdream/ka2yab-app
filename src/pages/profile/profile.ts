import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { IScrollTab, ScrollTabsComponent } from '../../components/scrolltabs';
import { WishProduct, CartProduct, Order, Address, Cart, Database } from '../../providers/database';
import { Storage } from '@ionic/storage';
import {User} from '../../user';
import {AuthenticatinService} from '../../authentication.service';
/**
 * Generated class for the Profile page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  tabs: IScrollTab[] = [
    {
      name: 'پروفایل',
      selected: true
    },

    // {
    //   name: 'لیست مورد علاقه',
    // },

  ];
  selectedTab: IScrollTab;
  @ViewChild('scrollTab') scrollTab: ScrollTabsComponent;
  db: Database;
  savedAddresses: Address[];
  wishProducts: WishProduct[];
  orders: Order[];
  cart: Cart;
  user = new User();
  isLogin = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private menu: MenuController,
    private storage:Storage,
    private authService: AuthenticatinService
    )
  {
    this.selectedTab = this.tabs[0];
    this.db = Database.getInstance();
    this.cart = Cart.getInstance();
    this.savedAddresses = this.db.allSavedAdddress();
    this.wishProducts = this.db.allWishList();
    this.orders = this.db.allOrders();
    // this.getUser();
    authService.emitChange.subscribe(text => {
      // if(typeof(text) === "boolean") {
      this.storage.get('user').then((user) => {
        console.log("user emit -----");
        console.log(user);
        if (user) {
          this.user = JSON.parse(user);
          this.isLogin = true;
          console.log(this.isLogin);
        }
        else {
          this.isLogin = false;
          this.user = new User();
        }
      });
      // }
    });

  }

  ionViewDidEnter() {
    this.menu.swipeEnable(false, 'ecom9');
    var detail = this.navParams.get('detail');
    console.log(detail);
    if (detail) {
      for (var i = 0; i < this.tabs.length; i++) {
        if (this.tabs[i].name.toLowerCase() === detail.toLowerCase()) {
          this.scrollTab.go2Tab(i);
          this.navParams.data.detail = undefined;
        }
      }
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CategoriesPage');
  }

  tabChange(data: any) {
    this.selectedTab = data.selectedTab;
  }

  swipeEvent($e) {
    console.log('before', $e.direction);
    switch ($e.direction) {
      case 2: // left
        this.scrollTab.nextTab();
        break;
      case 4: // right
        this.scrollTab.prevTab();
        break;
    }
  }

  editAddress(addr: Address) {

  }

  removeAddress(addr: Address) {
    this.db.removeSavedAddress(addr);
  }

  add2Cart(wish: WishProduct) {
    let cp: CartProduct;
    cp = {
      product: wish.product,
      quantity: 1,
      color: wish.color,
      size: wish.size,
    };

    let flgFound = false;
    this.cart.products.forEach(item => {
      if (item.product.id === cp.product.id) {
        flgFound = true;
        item.quantity = parseInt(item.quantity.toString()) + parseInt(cp.quantity.toString());
      }
    })
    if (!flgFound) {
      this.cart.products.push(cp);
    }
  }

  removeWish(wish: WishProduct) {
    this.db.removeWish(wish);
  }

  getUser() {
    this.storage.get('user').then((user) => {
      console.log("user");
      console.log(user);
      if(user){
        this.user = JSON.parse(user);
        this.isLogin = true;
      }
    });
  }
  signup(page){
    this.navCtrl.setRoot(page);
  }
}
