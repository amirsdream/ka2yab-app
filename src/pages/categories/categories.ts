import { Component, ViewChild, Pipe, PipeTransform } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, ModalController, Content } from 'ionic-angular';
import { IScrollTab, ScrollTabsComponent } from '../../components/scrolltabs';
import { Category, Database } from '../../providers/database';
import { LoadingController } from 'ionic-angular';
import {BrandService} from '../../brand.service' ;
import { Slider } from "../../slider";
import {baseUrl} from '../../baseurl';
import {AlldataService} from '../../alldata.service' ;
import {Category1} from '../../category';
import {Product} from '../../product';
import {Pricerange} from '../../pricerange';
import {Filtervalue} from '../../filtervalue';
import {FilterchangeService} from '../../filterchange.service';
import {Attribute} from '../../attribute';
import {Brand} from '../../brand';
import {Categoryattribute} from "../../categoryattribute";
import {CategorychangeService} from '../categorychange.service';
// @Pipe({ name: 'byCategory' })
// export class ByCategoryPipe implements PipeTransform {
//   transform(products: Product[], category: Category) {
//     return products.filter(product => {
//       return product.categories.indexOf(category) >= 0;
//     });
//   }
// }

/**
 * Generated class for the Categories page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-categories',
  templateUrl: 'categories.html',
})
export class CategoriesPage {
  range = new Pricerange();
  selectedRange = new Pricerange();
  values: Filtervalue[] = [];
  defaultValues: Filtervalue[] = [];
  attributes: Attribute[] = [];
  attribute = new Attribute();
  defaultAttributes: Attribute[] = [];
  brands:Brand[] = [];
  selectedBrands:Brand[] = [];

  productsOfCtaegoryURL = baseUrl + "api/category/products";
  categorySubsURL= baseUrl +"api/category/subs/all";
  productsOfCtaegoryFirstURL = baseUrl + "api/category/products/first";
  productsOfCtaegorySecondURL = baseUrl + "api/category/products/second";
  categoryFilteredAttributesUrl = baseUrl +"api/category/attributes/isfilter/home";
  categoryFilterAttributesUrl = baseUrl +"api/categoryattribute/attributes";
  category = new Category1() ;
  getProductsUnsubscribe: any;
  getSubCatUnsubscribe: any;
  imageUrl = baseUrl +"api/images";
  imageDir = "images/";
  baseUrl = baseUrl;
  products: Product[] = [];
  allProduct: Product[] = [];
  tabs: IScrollTab[] = [];
  selectedTab: IScrollTab;
  db: Database;

  categories: Category[] = Array<Category>();
  menus: Category = new Category();
  show: boolean = true;
  @ViewChild('scrollTab') scrollTab: ScrollTabsComponent;
  @ViewChild(Content) content: Content;

  productSubFirst: any;
  productSubSecond: any;
  navbarSub: any;
  order_id = 2;
  max = 0;
  pages: number[] = [];
  numberInPage = 12;
  loading = false ;
  category_id = 0;
  p = 1;

  categoryattributes:Categoryattribute[] = [];
  secondCategoryattributes:Categoryattribute[] = [];
  selectedCategoryattributes:Categoryattribute[] = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, private menu: MenuController, private modalCtrl: ModalController
    ,private brandService: BrandService, public loadingCtrl: LoadingController) {
    this.category = this.navParams.get('category');
    this.category_id = this.category.id;
  }

  ionViewDidEnter() {
    this.menu.swipeEnable(false, 'ecom9');
    this.show = true;
  }

  ionViewWillLeave() {
    this.menu.swipeEnable(true, 'ecom9');
    this.show = false;
  }

  ionViewDidLoad() {


    console.log('category----------------------------');
    console.log(this.navParams.get('category'));
    this.getCats();
    this.getDetailsFirst();

    // console.log('ionViewDidLoad CategoriesPage');
    // this.db = Database.getInstance();
    // this.products = this.db.allProduct();
    // var detail = this.navParams.get('select');
    // this.menus = this.navParams.get('menus');
    // if (this.menus) {
    //   this.categories = this.menus.children;
    //   this.menus.children.forEach(menu => {
    //     this.tabs.push({ name: menu.name });
    //   });
    //
    //   for (var i = 0; i < this.tabs.length; i++) {
    //     if (this.tabs[i].name.toLowerCase() === detail.toLowerCase()) {
    //       this.scrollTab.go2Tab(i);
    //     }
    //   }
    // }
  }

  tabChange(data: any) {
    // this.selectedTab = data.selectedTab;
    // this.content.scrollToTop();
    // console.log('data------------------------');
    // // console.log(data.selectedTab.id);
    this.category = data.selectedTab ;
    // this.tabs = [];
    // this.products = [];
    // this.p = 1;
    // this.selectedCategoryattributes = [];
    // this.order_id = 2;
    // this.selectedBrands = [];
    // this.getCats();
    // this.getDetailsFirst();
    this.navCtrl.push('CategoriesPage', {category: this.category});
      // alert(data.toString());
  }

  swipeEvent($e) {
    console.log('before', $e.direction);
    switch ($e.direction) {
      case 2: // left
        this.scrollTab.nextTab();
        break;
      case 4: // right
        this.scrollTab.prevTab();
        break;
    }
  }

  filterModal() {
    let modal = this.modalCtrl.create('FilterModalPage', {
      category: this.products,
      categoryattributes: this.categoryattributes,
      range:this.range ,
      selectedRange:this.selectedRange ,
      brands: this.brands,
      selectedCategoryattributes:this.selectedCategoryattributes,
      selectedBrands: this.selectedBrands,
      order_id: this.order_id,
    });
    console.log('categoryAttributes')
    console.log(this.categoryattributes);
    modal.onDidDismiss(data => {
      if (data) {
        console.log(data);
        this.selectedBrands = data.brands;
        this.selectedCategoryattributes = data.categoryattributes;
        let structure = data.structure;
        this.selectedRange.min = parseInt(structure.lower);
        this.selectedRange.max = parseInt(structure.upper);
        this.order_id = parseInt(data.order_id);
        this.p = 1;
        this.getDetailsSecond('first');
        console.log(data.brands)
        // this.products = data.products;
      }
    });
    modal.present();
  }

  toProduct(prod: Product) {
    this.navCtrl.push('ProductPage', { product: prod });
  }
  getCats() {

    // const loading = this.loadingCtrl.create({
    //
    //   content: `
    //   <div class="custom-spinner-container  ">
    //     <div class="custom-spinner-box  "></div>
    //   </div>`,
    // });
    // loading.present();

    this.getSubCatUnsubscribe = this.brandService.getOneObservable(this.categorySubsURL,this.category.id).subscribe((categories)=>{
      this.tabs = categories ;
      console.log('tabs---------------');
      console.log(this.tabs);
    });

    // this.getProductsUnsubscribe = this.brandService.getOneObservable(this.productsOfCtaegoryURL, this.category.id).subscribe(res => {
    //   this.products = res[0];
    //   // console.log('products--------');
    //   // console.log(this.products);
    //   loading.dismiss();
    // });
    // loading.dismiss();

  }
  doInfinite(event:any){
    if(this.loading == false){
      console.log("scrool");
      this.p += 1;
      this.getDetailsSecond('scroll',event);
      // this.products.forEach(pr => {
      //   this.products.push(pr)
      // });
      // event.complete();
    }
  }
  getDetailsFirst() {
    const loading = this.loadingCtrl.create({

      content: `
      <div class="custom-spinner-container  ">
        <div class="custom-spinner-box  "></div>
      </div>`,
    });
    loading.present();

    let reqNum = 0;
    this.loading = true ;
    if (this.productSubFirst !== undefined) {
      this.productSubFirst.unsubscribe();
    }
    this.productSubFirst = this.brandService.getOneObservable(this.productsOfCtaegoryFirstURL, this.category.id).subscribe(res => {
      this.loading = false ;
      console.log('products--------cat--------------');
      console.log(res);
      res.products.forEach((product,j)=> {
        let sumNumber = 0 ;
        let sumAll = 0 ;
        let lenColors = product.colors.length;
        product.colors.forEach((cl, i) => {
          sumNumber += parseInt(cl.number) ;
          sumAll  += parseInt(cl.all) ;
          if(i == lenColors - 1) {
            if( (sumNumber) > 0 ) {
              product.exist = 'موجود';
            }
            else {
              product.exist = 'ناموجود';
            }
          }

        });
      });
      this.pages = new Array(res.length);
      // this.secondProducts = res.products;
      this.products = res.products;
      this.category = res.category;
      this.brands = res.brands;
      this.max = res.max;
      this.range.max = res.max;
      this.range.min = 0;
      this.selectedRange.max =  res.max;
      this.selectedRange.min = 0;
      console.log('max---------  ' + res.max)
      reqNum +=1;
      if(reqNum == 2){
        loading.dismiss();
      }



    });
    this.brandService.getOneObservable(this.categoryFilteredAttributesUrl, this.category.id).subscribe((cat_attrs)=>{
      this.secondCategoryattributes = cat_attrs;
      let len = cat_attrs.length;
      // if( cat_attrs.length == 0) {
      //   this.categoryattributes = [];
      //   this.haveFilter = true;
      // }
      // else{
      //   this.haveFilter  = false;
      // }
      if(len == 0){
        loading.dismiss();
      }

      this.secondCategoryattributes.forEach((cat_attr, i) => {
        this.brandService.getOneObservable(this.categoryFilterAttributesUrl, cat_attr.id).subscribe((attributes)=>{
          cat_attr.attributes = attributes;
          if(i == len - 1){
            reqNum += 1;
            if(reqNum == 2 && len >0){
              loading.dismiss();
            }
            this.categoryattributes = this.secondCategoryattributes;
            console.log('category attributes-----------------');
            console.log(this.categoryattributes);
          }
        });
      });
    });
  }
  getDetailsSecond(role,event?) {
    const loading = this.loadingCtrl.create({

      content: `
      <div class="custom-spinner-container  ">
        <div class="custom-spinner-box  "></div>
      </div>`,
    });
    if(this.p == 1) {
      loading.present();
    }


    this.loading = true ;
    if (this.productSubSecond !== undefined) {
      this.productSubSecond.unsubscribe();
    }
    let formData = new FormData();
    formData.append('page', this.p.toString());
    formData.append('numberInPage', this.numberInPage.toString());
    formData.append('range', JSON.stringify(this.selectedRange));
    formData.append('categoryattributes', JSON.stringify(this.selectedCategoryattributes));
    formData.append('brands', JSON.stringify(this.selectedBrands));
    formData.append('category_id', this.category.id.toString());
    formData.append('order_id', this.order_id.toString());
    this.productSubSecond = this.brandService.createObservable(this.productsOfCtaegorySecondURL, formData).subscribe(res => {
      this.loading = false ;
      if(role == 'scroll') {
        setTimeout(()=> {
          event.complete();
          event.enable(false);
        },500);

        if(this.p < this.pages.length/this.numberInPage) {
          setTimeout(() => {
            event.enable(true);
          }, 1500)
          console.log(this.p)
          console.log(this.pages.length/this.numberInPage)
        }

      }
      console.log('roduct-------------cat--------');
      console.log(res);
      // this.p = res.currentPage;
      res.products.forEach((product,j)=> {
        let sumNumber = 0 ;
        let sumAll = 0 ;
        let lenColors = product.colors.length;
        product.colors.forEach((cl, i) => {
          sumNumber += parseInt(cl.number) ;
          sumAll  += parseInt(cl.all) ;
          if(i == lenColors - 1) {
            if( (sumNumber) > 0 ) {
              product.exist = 'موجود';
            }
            else {
              product.exist = 'ناموجود';
            }
          }

        });
      });

      this.pages = new Array(res.length);
      // this.secondProducts = res.products;
      if(role == 'first') {
        this.products = res.products;
      }
      else {
        let lenght = res.products.length;
        res.products.forEach((pr,i)=>{
          this.products.push(pr);
          // if(i == length-1) {
          //   event.enable(false);
          //   setTimeout(() => {
          //
          //     event.enable(true)
          //   }, 1500)
          // }
        });
      }
      if(this.p == 1) {
        loading.dismiss();
      }
      // let paginate = document.querySelectorAll('.ngx-pagination li');
      // ;
      // paginate[this.p - 1].classList.add('current');
      // console.log('paginate');
      // console.log(paginate);
      // this.secondProducts = res[0];
      // this.products = res[0];
      // this.category = res[1];
    });
  }
}
