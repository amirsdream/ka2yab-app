import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Cart } from '../../providers/database';
import { Storage } from '@ionic/storage';
import {baseUrl} from '../../baseurl';
import {BrandService} from '../../brand.service';
import {BrandServiceToken} from '../../servicetoken.service';
import {Product} from '../../product';
import {Image} from '../../image';
import {Color} from '../../color';
import {Category1} from '../../category';
import { LoadingController } from 'ionic-angular';
import {NotificationsService} from '../../addproduct.service';
import {ShowdetailsService} from '../../showdetails.service';
import {AuthenticatinService} from '../../authentication.service';
import {CartService} from '../../cart.service';
import {Service} from '../../service.service';
import {MegamenuService} from '../../megamenu.service';
import {NgForm} from "@angular/forms";

/**
 * Generated class for the Mycart page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-mycart',
  templateUrl: 'mycart.html',
})
export class MyCartPage {
  cart: Cart;
  products:Product[] = [];
  imageDir = "images/";
  showImageUrl:string =baseUrl+'images/';
  baseUrl = baseUrl;
  allCartNumber = 0;

  error = "";
  sendPrice:number = 0;
  buyURL = baseUrl +"api/buy";
  checkNumberURL = baseUrl +"api/color/check/number";
  sumPrice = 0;
  isLogin : boolean = false;
  loading = false ;
  loadingTemplate:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,private brandService: BrandService, public loadingCtrl: LoadingController
    ,private authService:AuthenticatinService, private cartService: CartService,private api:BrandServiceToken, private storage:Storage,) {
      this.cart = Cart.getInstance();
      this.products = this.cartService.productsOfCart$  ;
      console.log('products');
      console.log(this.cartService.productsOfCart$);

      this.checkPrice();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MycartPage');
  }

  placeOrder() {
    this.navCtrl.push('CheckoutPage');
  }

  incQty(product) {
    product.selectedColor.numberInCart = Number(product.selectedColor.numberInCart) + 1;
    this.cartService.emit(true);
    this.checkPrice();
    // item.quantity = parseInt(item.quantity) + 1;
  }

  decQty(product) {
    if (product.selectedColor.numberInCart > 1) {
      product.selectedColor.numberInCart = Number(product.selectedColor.numberInCart) - 1;
      this.cartService.emit(true);
      this.checkPrice();
    }
  }
  setCart() {
    this.allCartNumber = 0;
    let len = this.cartService.productsOfCart$.length;
    this.cartService.productsOfCart$.forEach((pr,i) => {
      let num = Number(this.allCartNumber) + Number(pr.selectedColor.numberInCart);
      console.log('cart --------------');
      console.log('all: ' + this.allCartNumber);
      console.log( 'slect: '+pr.selectedColor.numberInCart);
      console.log('num: '+num);
      this.allCartNumber = Number(this.allCartNumber) + Number(pr.selectedColor.numberInCart);
      if(i == len-1){
        this.cartService.emit(true);
        this.checkPrice();
      }
    });
    if(len == 0) {
      this.cartService.emit(true);
      this.checkPrice();
    }
  }

  delete(id){
    this.products.forEach((product,i)=>{
      if(product.id == id){
        this.products.splice(i,1);
        sessionStorage["products"] = JSON.stringify(this.products);
        // this.deleteservice.emit(true);
        this.checkPrice();
      }
    });
  }
  checkPrice(){
    this.sumPrice = 0;
    this.cartService.productsOfCart$.forEach((product)=>{
      product.priceNumber=parseInt(product.selectedColor.price);
      this.sumPrice += product.priceNumber*product.selectedColor.numberInCart;
      console.log('price check----------------');
      console.log(product.selectedColor.price)
      console.log(product.priceNumber)
    });
  }
  plusNumber(i:any){
    // if(this.products[i].selectedColor.number > this.products[i].selectedColor.numberInCart){
    this.products[i].selectedColor.numberInCart += 1;
    sessionStorage["products"] = JSON.stringify(this.products);
    this.checkPrice();
    // this.products.forEach((product,i)=>{
    //   if(product.id == id){
    //     this.products.splice(i,1);
    //     sessionStorage["products"] = JSON.stringify(this.products);
    //     this.deleteservice.emit(true);
    //   }
    // });
    // this.product.selectedColor.numberInCart = this.
  }
  minusNumber(i:any){
    if(this.products[i].selectedColor.numberInCart > 1){
      this.products[i].selectedColor.numberInCart -= 1;
      sessionStorage["products"] = JSON.stringify(this.products);
      this.checkPrice();
    }
  }
  checkNumber(){
    this.error = '';
    this.loadingTemplate = this.loadingCtrl.create({
      content: `
      <div class="custom-spinner-container  ">
        <div class="custom-spinner-box  "></div>
      </div>`,
    });
    this.storage.get('user').then((user) => {
      this.storage.get('token').then((token) => {
        if(token != null) {
          let len = this.products.length;
          this.loading = true;
          this.loadingTemplate.present();
          this.products.forEach((product, i) => {
            let formData = new FormData();
            formData.append('product', JSON.stringify(product));
            console.log('token----------');
            console.log(token);
            formData.append('token', token);
            this.api.createObservable(this.checkNumberURL, formData,token).subscribe((res) => {
              console.log("res------")
              console.log(res)
              if (res.result == false) {
                // console.log(this.products);
                this.error += `موجودی کالا ${product.name} کافی نیست`;
                this.error += "\n";
                console.log("error set : " + this.error);
              }

              if (this.error.length == 0 && i == len - 1) {
                this.send();
              }
              if (this.error.length > 0 && i == len - 1) {
                alert(this.error);
                this.error = "";
                this.loading = false;
                this.loadingTemplate.dismiss();
              }
            });
          });
          // if(len == 0) {
          //   this.loadingTemplate.dismiss();
          //   this.loading = true;
          // }
          console.log("error");
          console.log(this.error);
        }
        else {
          alert("برای خرید لطفا لاگین کنید");
        }
      });
    });
  }
  send(){
    this.storage.get('user').then((user) => {
      this.storage.get('token').then((token) => {
        console.log('user----------------')
        console.log(user);
        if (user != null) {
          let formData = new FormData();
          this.products.forEach((product, i) => {
            let index = "products[" + i + "]";
            formData.append(index, JSON.stringify(product));
          });
          formData.append('user', user);
          //formData.append('token', token);
          formData.append('sum_price', this.sumPrice.toString());
          this.api.createObservable(this.buyURL, formData,token).subscribe(res => {
            console.log(res);
            if(res != 'you are not login'){
              this.products = [];
              // sessionStorage["products"] = JSON.stringify(this.products);
              // this.notificationsService.emit(true);
              // this.router.navigate(['/home']);
              alert("خرید شما با موفقیت انجام شد");
              this.cartService.productsOfCart$ = [];
              this.cartService.emit(true);
              this.navCtrl.setRoot('TabsPage');
              this.loading = false;
              this.loadingTemplate.dismiss();
            }
            else {
              alert("برای خرید لطفا لاگین کنید");
              this.loading = false;
              this.loadingTemplate.dismiss();
            }


          });
        }
        else {
          alert("برای خرید لطفا لاگین کنید");
          this.loading = false;
          this.loadingTemplate.dismiss();
        }
      });
    });
    // let formData = new FormData();
    // //let user = JSON.parse(sessionStorage.getItem('user'));
    // if(this.isLogin){
    //   // formData.append('products',JSON.stringify(this.products));
    //   this.products.forEach((product,i) => {
    //     let index = "products["+i+"]";
    //     formData.append(index,JSON.stringify(product));
    //   });
    //   formData.append('user',sessionStorage.getItem('user'));
    //   formData.append('token',sessionStorage.getItem('token'));
    //   formData.append('sum_price',this.sumPrice.toString());
    //   this.api.createObservable(this.buyURL,formData).subscribe(res=>{
    //     console.log(res);
    //     this.products = [];
    //     sessionStorage["products"] = JSON.stringify(this.products);
    //     // this.notificationsService.emit(true);
    //     // this.router.navigate(['/home']);
    //     alert("خرید شما با موفقیت انجام شد");
    //     this.loading = false ;
    //
    //   });
    // }
    // else{
    //   alert("برای خرید لطفا لاگین کنید");
    //   // this.router.navigate(['/signin']);
    // }
  }
  setStylePrice(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }
  deleteProduct(i) {
    this.products.splice(i,1);
    this.cartService.productsOfCart$.splice(i,1);
    this.setCart();
  }
}
