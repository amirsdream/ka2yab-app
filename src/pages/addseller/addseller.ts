import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {NgForm} from '@angular/forms';
import { LoadingController } from 'ionic-angular';
import { Address } from '../../iran-address';
import {baseUrl} from '../../baseurl';
import {User} from '../../user';
import {BrandService} from '../../brand.service';

/**
 * Generated class for the Signup page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-addseller',
  templateUrl: 'addseller.html',
})
export class AddsellerPage {

  resultCaptcha = false;
  error = "";
  tokenCaptcha = null ;
  filesToUpload: Array<File> = [];
  urlExistEmail=baseUrl+'api/exist/email/home';
  marketerCityUrl:string = baseUrl+'api/users/marketer/city';
  agentCityUrl:string = baseUrl+'api/users/agent/marketer/city/home';
  urlUser:string=baseUrl+'api/users/home';
  // email = "" ;
  existEmail  = false ;
  samePassword = false ;

  address = new Address();
  users:User[] = [];
  selectedUsers:User[] = [];
  agents:User[]  = [];
  marketers:User[] = [];
  secondMarketers:User[] = [];
  agent = new User();
  marketer = new User();
  numbers:number[] = [];
  states = [];
  cities = [];
  city:string="";
  province:string="";
  stateChange = false ;
  stateEmpty = false ;
  cityEmpty = false ;
  cityChange = false ;
  loading = false ;

  mimeTypeError = false ;
  emptyFileError = false ;

  constructor(private brandService:BrandService, public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController) {
    this.states = this.address.states ;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }

  skip() {
    this.navCtrl.setRoot('TabsPage');
  }

  register() {
    this.navCtrl.setRoot('TabsPage');
  }


  changeState(input:any){
    // console.log(input)
    this.stateChange = true ;
    if(input == '') {
      this.stateEmpty = true ;
    }
    else {
      this.stateEmpty = false ;
    }
    console.log(input);
    this.cities = this.address.loadCity(input);
    // alert("salamsdfdfadweddd")
    // this.city = this.cities[0];
    console.log(this.city + this.province);
    this.getMarketer();
  }
  changeCity(input:any) {
    this.cityChange = true ;
    if ( input === '' ) {
      this.cityEmpty = true ;
    }
    else {
      this.cityEmpty = false ;
    }
    console.log(input);
    // this.cities = this.address.loadCity(input.target.value)
    // alert("salamsdfdfadweddd")
    this.city = input;
    console.log(this.city + this.province)
    this.getMarketer();
  }
  checkSamePassword(password, passRepeat) {
    setTimeout(() => {
      console.log(password.target.value + '=' + passRepeat) ;
      if(password.target.value != passRepeat && password.target.value != "" && passRepeat != "") {
        this.samePassword = true ;
      }
      else {
        this.samePassword = false ;
      }
    },200);

  }
  checkSamePasswordRepeat(password, passRepeat) {
    setTimeout(() => {
      console.log(password+ '=' + passRepeat.target.value) ;
      if(password != passRepeat.target.value && password != "" && passRepeat.target.value != "") {
        this.samePassword = true ;
      }
      else {
        this.samePassword = false ;
      }
      console.log(this.samePassword);
    },200);

  }
  emailKeyDown(event: any ) {
    this.existEmail = false ;
  }
  upload(f , firstName,lastName,email,pass,repeatPas,phone,mobile,address,zip_code,shoppingName){
    this.error = "";
    const loading = this.loadingCtrl.create({
      content: `
      <div class="custom-spinner-container  ">
        <div class="custom-spinner-box  "></div>
      </div>`,
    });

    console.log("form send");
    const formData: any = new FormData();
    // console.log(f.controls.email.dirty) ;
    console.log(f.valid) ;
    const files: Array<File> = this.filesToUpload;
    // formData.append("file", files[0], files[0]['name']);



    //
    if(pass != repeatPas && pass != "" && repeatPas != ""){
      this.error  +=  "پسورد و تکرار پسورد یکسان نیستند.\n";
    }

    if(this.province == '') {
      this.stateEmpty = true ;
      this.error  +=  "فیلد استان خالی می باشد.\n";

    }
    else {
      this.stateEmpty = false ;
    }
    if(this.city == '') {
      this.cityEmpty = true ;
      this.error  +=  "فیلد شهر خالی می باشد.\n";
    }
    else {
      this.cityEmpty = false ;
    }
    console.log( this.province + this.city + this.cityEmpty + this.stateEmpty)
    // if(firstName == ""){
    //     this.error  +=  "فیلد نام خالی می باشد.\n";
    // }
    // if(lastName == ""){
    //     this.error  +=  "فیلد نام خانوادگی خالی می باشد.\n";
    // }
    // if(email == ""){
    //     this.error  +=  "فیلد ایمیل خالی می باشد.\n";
    // }
    // if( pass == ""){
    //     this.error  +=  "فیلد پسورد خالی می باشد.\n";
    // }
    // if(mobile == ""){
    //     this.error  +=  "فیلد موبایل خالی می باشد.\n";
    // }
    // if(phone == ""){
    //     this.error  +=  "فیلد تلفن خالی می باشد.\n";
    // }
    //
    // if(address == ""){
    //     this.error  +=  "فیلد آدرس خالی می باشد.\n";
    // }
    // if(zip_code == ""){
    //     this.error  +=  "فیلد کدپستی خالی می باشد.\n";
    // }
    if( email != '' ) {
      let formDataEmail = new FormData();
      formDataEmail.append('email', email);
      this.loading = true ;
      loading.present();
      this.brandService.createObservable(this.urlExistEmail, formDataEmail).subscribe((res) => {
        //alert(res.exist)
        if (res.exist == true) {
          this.existEmail = true ;
          this.error = "این ایمیل قبلا ثبت شده است.\n";
          // alert(this.error);
          // this.error = "";
          loading.dismiss();
          this.loading = false ;
        }
        else {
          // console.log("token captcha === " + this.resultCaptcha) ;
          // console.log("length token " + this.captcha.getResponse().length ) ;
          if (this.error == ""  && f.valid && !this.stateEmpty && !this.cityEmpty && !this.mimeTypeError && !this.emptyFileError) {
            // console.log( "send token == " + this.captcha.getResponse() ) ;
            formData.append("firstName", firstName);
            formData.append("lastName", lastName);
            formData.append("email", email);
            formData.append("password", pass);
            formData.append("mobile", mobile);
            formData.append("role", "seller");
            formData.append("phone", phone);
            formData.append("active",false);
            formData.append("agent", JSON.stringify(this.agent));
            formData.append("marketer", JSON.stringify(this.marketer));
            console.log('file-------------');
            console.log(files);
            console.log(this.filesToUpload);
            formData.append("file", files[0], files[0]['name']);
            formData.append("shopping_name", shoppingName);
            formData.append("address", address);
            formData.append("zip_code", zip_code);
            formData.append("state", this.province);
            formData.append("city", this.city);
            this.brandService.createObservable(this.urlUser, formData).subscribe(
              res => {
                // alert("عملیات شما با موفقیت انجام شد");
                this.loading = false ;
                // this.router.navigate(['/success']);
                this.navCtrl.setRoot('TabsPage');
                loading.dismiss();
                // alert("Success") ;
                console.log("success") ;
              });
          }
          else {
            // alert(this.error);
            console.log("empty city");
            console.log(this.emptyFileError)
            console.log(this.cityEmpty)
            console.log(this.stateEmpty)

            this.error = "";
            this.loading = false ;
            loading.dismiss();
            //console.log( this.captcha.getResponse() ) ;
          }
        }
      });
    }
    else {
      //alert(this.error);
      //this.error = '';
      console.log('email empty')
    }
  }
  add(){
    // let value = new Filtervalue();
    this.numbers.push(0);
  }
  delete(i:number){
    if( this.numbers.length >1 ){
      this.numbers.splice(i,1);
      this.selectedUsers.splice(i,1);
      // console.log(this.selectedUsers)
    }
  }
  getMarketer(){
    let formData = new FormData();
    formData.append("state",this.province);
    formData.append("city",this.city);
    const loading = this.loadingCtrl.create({
      content: `
      <div class="custom-spinner-container  ">
        <div class="custom-spinner-box  "></div>
      </div>`,
    });
    // alert("salamamakjakljehfdlkad")
    //  alert(this.city+this.province)
    loading.present();
    this.brandService.createObservable(this.agentCityUrl,formData).subscribe(
      res=>{
        loading.dismiss();
        // this.users = res;
        // this.selectedUsers = [];
        // if(res.length>0){
        //   this.selectedUsers.push(res[])
        // }
        this.agents = res.agents;
        // this.marketers = res.marketers;
        this.secondMarketers = res.marketers;
        // this.agent = this.agents[0];
        // this.marketer = this.marketers[0];

        console.log("agent marketer ----------------------")
        console.log(res);
        // alert("عملیات شما با موفقیت انجام شد");
        // this.router.navigate(['/dashboard/user/show']);
      });
  }
  changeMarketer(input:any){

    this.marketer=this.marketers.filter(user=>user.id == input)[0];
    console.log(this.marketer);
  }
  changeAgent(input:any){
    this.agent=this.agents.filter(user=>user.id == input)[0];
    this.marketers = this.secondMarketers.filter(user => user.agent_id == this.agent.id);
    console.log(this.agent)
  }
  fileChangeEvent(fileInput: any) {
    console.log("file input----");
    console.log(fileInput);
    if ( fileInput.target.files.length > 0 ) {
      if ( fileInput.target.files[0].type === 'image/jpeg' || fileInput.target.files[0].type === 'image/png' || fileInput.target.files[0].type === 'image/jpg') {
        this.mimeTypeError = false ;
      } else {
        this.mimeTypeError = true ;
      }
      this.emptyFileError = false ;
    } else {
      this.emptyFileError = true ;
    }
    this.filesToUpload = fileInput.target.files ;
    //    console.log(this.filesToUpload);
  }

}
