import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddsellerPage } from './addseller';

@NgModule({
  declarations: [
    AddsellerPage,
  ],
  imports: [
    IonicPageModule.forChild(AddsellerPage),
  ],
  exports: [
    AddsellerPage
  ]
})
export class SignupModule {}
