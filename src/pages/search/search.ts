import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {  Database } from '../../providers/database';
import {Product} from '../../product';
import {Image} from '../../image';
import {Brand} from '../../brand';
import {Category1} from '../../category';
import {Attribute} from '../../attribute';
import {Productcategory} from '../../productcategory';
import {User} from '../../user';
import {Menu} from '../../menu';
import { LoadingController } from 'ionic-angular';
import {baseUrl} from '../../baseurl';
import {BrandService} from '../../brand.service';
import {AlldataService} from '../../alldata.service';
import {Pricerange} from '../../pricerange';
/**
 * Generated class for the Search page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {

  searching = false ;
  loading = false ;
  imageDir = 'images/';
  baseUrl = baseUrl;
  searchURL = baseUrl + 'api/search';
  searchSecondURL = baseUrl + 'api/search/second';
  searchFilterURL = baseUrl + 'api/search/filter';

  products: Product[] = [];
  secondProducts: Product[] = [];
  searchText = '';
  searchUnsubscribe: any;
  loadingTemplate:any;
  pages:number[] = [];
  productSubSecond:any;
  productSubFilter:any;
  max:number =0;
  range = new Pricerange();
  numberInPage = 12;
  p = 1;
  results: Product[];
  mark: string;
  constructor(public navCtrl: NavController, public navParams: NavParams,private api: BrandService,public loadingCtrl: LoadingController) {
    this.results = new Array<Product>();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchPage');
  }

  initializeItems() {
    let db = Database.getInstance();
    // this.results = db.allProduct();
  }

  getItems(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the searchbar
    let val = ev.target.value;
    console.log('val = '+val)
    this.searchText = val;
    if(val != undefined) {
      this.search(null);
    }
    // if the value is an empty string don't filter the items
    // if (val && val.trim() != '') {
    //   this.mark = val;
    //   this.results = this.results.filter((item) => {
    //     return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
    //   });
    //   console.log(this.results);
    // } else {
    //   this.results = [];
    // }
  }

  decorateTitle(title: string): string {
    let regEx = new RegExp(this.mark, 'ig')
    let str = title.replace(regEx, `<span>${this.mark}</span>`);
    return str;
  }

  toProduct(prod: Product) {
    this.navCtrl.push('ProductPage', {product: prod});
  }
  doInfinite(event:any){
    if(this.loading == false){
      console.log("scrool");
      this.p += 1;
      this.getDetailsFilter('scroll',event);
      // this.products.forEach(pr => {
      //   this.products.push(pr)
      // });
      // event.complete();
    }
  }
  search(event: any) {
    // if (this.loadingTemplate != undefined){
    //   this.loadingTemplate.dismiss();
    // }
    // let  loadingTemplate = this.loadingCtrl.create({
    //   content: `
    //   <div class="custom-spinner-container  ">
    //     <div class="custom-spinner-box  "></div>
    //   </div>`,
    // });
    this.searching = true ;
    this.products =  [] ;
    this.getDetailsSecond('first');
    // setTimeout(() => {
    //   // console.log(this.searchText);
    //   if (this.searchText.trim().length > 1) {
    //     if ( this.searchUnsubscribe != undefined) {
    //       this.searchUnsubscribe.unsubscribe();
    //     }
    //     this.loading = true;
    //     const formData = new FormData() ;
    //     formData.append('searchText', this.searchText) ;
    //     // loadingTemplate.present();
    //     this.searchUnsubscribe = this.api.createObservable(this.searchURL , formData).subscribe((products) => {
    //       // loadingTemplate.dismiss();
    //       console.log(Array.isArray(products));
    //       if ( Array.isArray(products) ) {
    //         this.products = products;
    //         this.loading = false;
    //         // sessionStorage['searchProducts'] = JSON.stringify(this.products) ;
    //         // console.log('search products') ;
    //         console.log(this.products) ;
    //       }
    //       else {
    //         this.products = Object.keys(products).map(function(key) {
    //           return  products[key] ;
    //         });
    //         this.loading = false ;
    //         // sessionStorage['searchProducts'] = JSON.stringify(this.products) ;
    //         // console.log(this.products) ;
    //       }
    //       this.searching  = false;
    //       // this.loadingTemplate.dismiss();
    //       // this.router.navigate(['/search/interface']) ;
    //
    //     });
    //   }
    //   else{
    //     this.products = [];
    //     //console.log("keypress")
    //   }
    //
    // }, 100) ;
  }
  getDetailsSecond(role,event?) {
    // console.log(sessionStorage['searchText']);
    if (this.searchText.length > 1) {
      this.loading = true;
      if (this.productSubSecond !== undefined) {
        this.productSubSecond.unsubscribe();
      }
      let formData = new FormData();
      formData.append('searchText', this.searchText);
      this.productSubSecond = this.api.createObservable(this.searchSecondURL, formData).subscribe(res => {
        this.loading = false;
        console.log('products--------brand--------------');
        console.log(res);
        res.products.forEach((product,j) => {
          let sumNumber = 0 ;
          let sumAll = 0 ;
          let lenColors = product.colors.length;
          product.colors.forEach((cl, i) => {
            sumNumber += parseInt(cl.number) ;
            sumAll  += parseInt(cl.all) ;
            if(i == lenColors - 1) {
              if( (sumNumber) > 0 ) {
                product.exist = 'موجود';
              }
              else {
                product.exist = 'ناموجود';
              }
            }

          });
        });
        this.pages = new Array(res.length);
        console.log(res.length);
        console.log(this.pages);
        this.secondProducts = res.products;
        this.products = res.products;
        // this.category = res.category;
        // this.brand = res.brand;
        this.max = res.max;
        this.range.max = res.max;
        this.range.min = 0;
      });
    }
    else {
      this.products = [];
    }
  }
  getDetailsFilter(role,event?) {
    if (this.searchText.length > 1) {
      this.loading = true;
      if (this.productSubFilter !== undefined) {
        this.productSubFilter.unsubscribe();
      }
      let formData = new FormData();
      formData.append('page', this.p.toString());
      formData.append('searchText', this.searchText);
      formData.append('numberInPage', this.numberInPage.toString());
      formData.append('range', JSON.stringify(this.range));
      // formData.append('categoryattributes', JSON.stringify(this.categoryattributes));
      // formData.append('brands', JSON.stringify(this.selectedBrands));
      // formData.append('brand_id', this.brand_id.toString());
      // formData.append('order_id', this.order_id.toString());
      this.productSubFilter = this.api.createObservable(this.searchFilterURL, formData).subscribe(res => {
        this.loading = false;
        console.log('roduct-------------cat--------');
        console.log(res);

        if(role == 'scroll') {
          setTimeout(()=> {
            event.complete();
            event.enable(false);
          },500);

          if(this.p < this.pages.length/this.numberInPage) {
            setTimeout(() => {
              event.enable(true);
            }, 1500)
            console.log(this.p)
            console.log(this.pages.length/this.numberInPage)
          }

        }
        // this.p = res.currentPage;
        res.products.forEach((product,j) => {
          let sumNumber = 0 ;
          let sumAll = 0 ;
          let lenColors = product.colors.length;
          product.colors.forEach((cl, i) => {
            sumNumber += parseInt(cl.number) ;
            sumAll  += parseInt(cl.all) ;
            if(i == lenColors - 1) {
              if( (sumNumber) > 0 ) {
                product.exist = 'موجود';
              }
              else {
                product.exist = 'ناموجود';
              }
            }

          });
        });
        this.pages = new Array(res.length);
        // console.log()
        if(role == 'first') {
          this.secondProducts = res.products;
          this.products = res.products;
        }
        else {
          res.products.forEach(pr=>{
            this.products.push(pr);
          });
        }

        // let paginate = document.querySelectorAll('.ngx-pagination li');
        // ;
        // paginate[this.p - 1].classList.add('current');
        // console.log('paginate');
        // console.log(paginate);
        // this.secondProducts = res[0];
        // this.products = res[0];
        // this.category = res[1];
      });
    }
    else {
      this.products = [];
    }
  }
  onBlurSearch(event: any) {
    this.searching = false ;
    console.log("blur")
  }
  onFocusSearch(event: any) {
    console.log("focus")
    this.searching = true ;
  }
  setStylePrice(x)  {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }
}
