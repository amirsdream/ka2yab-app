import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddmarketerPage } from './addmarketer';

@NgModule({
  declarations: [
    AddmarketerPage,
  ],
  imports: [
    IonicPageModule.forChild(AddmarketerPage),
  ],
  exports: [
    AddmarketerPage
  ]
})
export class SignupModule {}
