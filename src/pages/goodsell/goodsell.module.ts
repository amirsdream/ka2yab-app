import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {  GoodsellPage } from './goodsell';
import { ScrollTabsComponentModule } from '../../components/scrolltabs';

@NgModule({
  declarations: [
    GoodsellPage,

  ],
  imports: [
    ScrollTabsComponentModule,
    IonicPageModule.forChild(GoodsellPage),
  ],
  exports: [
    GoodsellPage
  ]
})
export class CategoriesModule {}
