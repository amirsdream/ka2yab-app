import { Component, ViewChild, Pipe, PipeTransform } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, ModalController, Content } from 'ionic-angular';
import { IScrollTab, ScrollTabsComponent } from '../../components/scrolltabs';
import { Category, Database } from '../../providers/database';
import {BrandService} from '../../brand.service' ;
import { Slider } from "../../slider";
import {baseUrl} from '../../baseurl';
import {AlldataService} from '../../alldata.service' ;
import {Category1} from '../../category';
import {Product} from '../../product';
import {Pricerange} from '../../pricerange';
//
// @Pipe({ name: 'byGoodsell' })
// export class ByGoodsell implements PipeTransform {
//   transform(products: Product[], category: Category) {
//     return products.filter(product => {
//       return product.categories.indexOf(category) >= 0;
//     });
//   }
// }

/**
 * Generated class for the Categories page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-goodsell',
  templateUrl: 'goodsell.html',
})
export class GoodsellPage {

  goodsellUrl:string = baseUrl+'api/products/goodsells/all';
  goodSellProductFirstUrl = baseUrl +"api/goodsell/first";
  goodSellProductSecondUrl = baseUrl +"api/goodsell/second";
  imageUrl = baseUrl +"api/images";
  imageDir = "images/";
  baseUrl = baseUrl;

  tabs: IScrollTab[] = [];
  selectedTab: IScrollTab;
  db: Database;
  products: Product[] = [];
  secondProducts : Product[] = [];
  categories: Category[] = Array<Category>();
  menus: Category = new Category();
  show: boolean = true;
  loading = false;
  pages:number[] = [];
  p = 1;
  productSubFirst:any;
  productSubSecond:any;
  order_id = 2;
  numberInPage  = 12;
  range = new Pricerange();
  max = 0;
  @ViewChild('scrollTab') scrollTab: ScrollTabsComponent;
  @ViewChild(Content) content: Content;

  constructor(public navCtrl: NavController, public navParams: NavParams, private menu: MenuController, private modalCtrl: ModalController,
              private brandService: BrandService) {
    //this.products = this.products.filter(pr => pr.categories)
    console.log("products-----------------------------------")
    console.log(this.products) ;
    console.log(this.categories) ;
  }
  ngOnInit() {
    this.brandService.getObservable(this.goodsellUrl).subscribe((products) => {
      this.products = products ;
      console.log("products good sell") ;
      console.log(this.products) ;
    }) ;
  }
  ionViewDidEnter() {
    this.menu.swipeEnable(false, 'ecom9');
    this.show = true;
  }

  ionViewWillLeave() {
    this.menu.swipeEnable(true, 'ecom9');
    this.show = false;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CategoriesPage');
    // this.db = Database.getInstance();
    // this.products = this.db.allProduct();
    console.log("products-----------------------------------")
    console.log(this.products) ;
    console.log(this.categories) ;
    // this.db = Database.getInstance();
    // this.products = this.db.allProduct();
    // var detail = this.navParams.get('select');
    // this.menus = this.navParams.get('menus');
    // if (this.menus) {
    //   this.categories = this.menus.children;
    //   this.menus.children.forEach(menu => {
    //     this.tabs.push({ name: menu.name });
    //   });
    //
    //   for (var i = 0; i < this.tabs.length; i++) {
    //     if (this.tabs[i].name.toLowerCase() === detail.toLowerCase()) {
    //       this.scrollTab.go2Tab(i);
    //     }
    //   }
    // }
  }
  doInfinite(event:any){
    if(this.loading == false){
      console.log("scrool");
      this.p += 1;
      this.getDetailsSecond('scroll',event);
      // this.products.forEach(pr => {
      //   this.products.push(pr)
      // });
      // event.complete();
    }
  }
  getDetailsFirst() {
    this.loading = true ;
    if (this.productSubFirst !== undefined) {
      this.productSubFirst.unsubscribe();
    }
    this.productSubFirst = this.brandService.getObservable(this.goodSellProductFirstUrl).subscribe(res => {
      this.loading = false ;
      console.log('products--------brand--------------');
      console.log(res);
      res.products.forEach((product,j) => {
        let sumNumber = 0 ;
        let sumAll = 0 ;
        let lenColors = product.colors.length;
        product.colors.forEach((cl, i) => {
          sumNumber += parseInt(cl.number) ;
          sumAll  += parseInt(cl.all) ;
          if(i == lenColors - 1) {
            if( (sumNumber) > 0 ) {
              product.exist = 'موجود';
            }
            else {
              product.exist = 'ناموجود';
            }
          }

        });
      });
      this.pages = new Array(res.length);
      console.log(res.length);
      console.log(this.pages)
      this.secondProducts = res.products;
      this.products = res.products;
      // this.category = res.category;
      // this.brand = res.brand;
      this.max = res.max;
      this.range.max = res.max;
      this.range.min = 0;
    });
  }
  getDetailsSecond(role,event?) {
    this.loading = true ;
    if (this.productSubSecond !== undefined) {
      this.productSubSecond.unsubscribe();
    }
    let formData = new FormData();
    formData.append('page', this.p.toString());
    formData.append('numberInPage', this.numberInPage.toString());
    formData.append('range', JSON.stringify(this.range));
    // formData.append('categoryattributes', JSON.stringify(this.categoryattributes));
    // formData.append('brands', JSON.stringify(this.selectedBrands));
    // formData.append('brand_id', this.brand_id.toString());
    formData.append('order_id', this.order_id.toString());
    this.productSubSecond = this.brandService.createObservable(this.goodSellProductSecondUrl, formData).subscribe(res => {
      this.loading = false ;
      console.log('roduct-------------cat--------');
      console.log(res);


      if(role == 'scroll') {
        setTimeout(()=> {
          event.complete();
          event.enable(false);
        },500);

        if(this.p < this.pages.length/this.numberInPage) {
          setTimeout(() => {
            event.enable(true);
          }, 1500)
          console.log(this.p)
          console.log(this.pages.length/this.numberInPage)
        }

      }

      res.products.forEach((product,j) => {
        let sumNumber = 0 ;
        let sumAll = 0 ;
        let lenColors = product.colors.length;
        product.colors.forEach((cl, i) => {
          sumNumber += parseInt(cl.number) ;
          sumAll  += parseInt(cl.all) ;
          if(i == lenColors - 1) {
            if( (sumNumber) > 0 ) {
              product.exist = 'موجود';
            }
            else {
              product.exist = 'ناموجود';
            }
          }

        });
      });
      // this.p = res.currentPage;
      this.pages = new Array(res.length);
      // console.log()

      if(role == 'first') {
        this.secondProducts = res.products;
        this.products = res.products;
      }
      else {
        res.products.forEach(pr=>{
          this.products.push(pr);
        });
      }
      // this.secondProducts = res.products;
      // this.products = res.products;
      // let paginate = document.querySelectorAll('.ngx-pagination li');
      // ;
      // paginate[this.p - 1].classList.add('current');
      // console.log('paginate');
      // console.log(paginate);
      // this.secondProducts = res[0];
      // this.products = res[0];
      // this.category = res[1];
    });
  }
  tabChange(data: any) {
    this.selectedTab = data.selectedTab;
    this.content.scrollToTop();
    console.log("products-----------------------------------")
    console.log(this.products) ;
    console.log(this.categories) ;

  }

  swipeEvent($e) {
    console.log('before', $e.direction);
    switch ($e.direction) {
      case 2: // left
        this.scrollTab.nextTab();
        break;
      case 4: // right
        this.scrollTab.prevTab();
        break;
    }
  }

  filterModal() {
    let modal = this.modalCtrl.create('FilterModalPage', { products: this.products });
    modal.onDidDismiss(data => {
      if (data) {
        console.log(data);
        this.products = data.products;
      }
    });
    modal.present();
  }

  toProduct(prod: Product) {
    this.navCtrl.push('ProductPage', { product: prod });
  }
}
