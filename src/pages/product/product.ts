import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Select } from 'ionic-angular';
import {  Cart, Database } from '../../providers/database';

import {baseUrl} from '../../baseurl';
import {BrandService} from '../../brand.service';
import {BrandServiceToken} from '../../servicetoken.service';
import {Product} from '../../product';
import {Image} from '../../image';
import {Color} from '../../color';
import {Category1} from '../../category';
import {User} from '../../user';
import { LoadingController } from 'ionic-angular';
import {NotificationsService} from '../../addproduct.service';
import {ShowdetailsService} from '../../showdetails.service';
import {AuthenticatinService} from '../../authentication.service';
import {CartService} from '../../cart.service';
import {Service} from '../../service.service';
import {MegamenuService} from '../../megamenu.service';
import {NgForm} from "@angular/forms";
/**
 * Generated class for the Product page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-product',
  templateUrl: 'product.html',
})
export class ProductPage {

  islogin:boolean = false;
  starsCount: number=4;
  numberProduct:number = 1 ;
  urlProduct = baseUrl +"api/products";
  imageUrl = baseUrl +"api/images";
  productcategoryUrl:string =baseUrl+'api/productcategories';
  attributeUrl:string =baseUrl+'api/attributes';
  importantAttributeUrl:string =baseUrl+'api/product/attributes/important';
  urlProductAllDataGroup = baseUrl +"api/products/all/data/group";
  navbarUrl:string =baseUrl+'api/product/categories/navbar';
  pointUserUrl:string =baseUrl+'api/product/point/user';
  categoryUrl:string =baseUrl+'api/categories';
  brandUrl:string =baseUrl+'api/brand';
  plusViewUrl:string =baseUrl+'api/product/view';
  productColorsUrl:string =baseUrl+'api/product/colors';
  pointUrl:string = baseUrl+'api/product/set/point';
  avgPointUrl = baseUrl+'api/product/average/point';
  commentUrl = baseUrl+'api/comments';
  exist:string="";
// product/categories/navbar
  imageDir = "images/";
  showImageUrl:string =baseUrl+'images/';
  baseUrl = baseUrl;
  message:string = "";
  product_id:number;
  product:Product=new Product();
  products: Product[] = [];
  productSecond:Product=new Product();
  selectedColor = new Product();
  colors:Color[] = [];
  price:string = "";
  navbars:Category1[] = [];
  numberInCart:number = 1;
  allCartNumber = 0;

  cb: boolean[] = [];
  size: boolean[] = [false, true, false, false, false]
  @ViewChild('qtySelect') qtySelect: Select;

  currentQty: string = 'تعداد: 1';
  quantity: number = 1;
  currentColor: string;
  currentSize: string;
  hideIt: boolean = true;
  tabBarElement: any;
  // product: Product;
  cart: Cart;
  db: Database;
  love = false;
  users: User[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams ,private brandService: BrandService, public loadingCtrl: LoadingController
  ,private authService:AuthenticatinService, private cartService: CartService) {
    // this.tabBarElement = document.querySelector('.tabbar.show-tabbar');
    this.product = this.navParams.get('product');
    this.product_id = this.product.id;
    this.setCart();
    // this.db = Database.getInstance();
    // this.cart = Cart.getInstance();
    // if (this.product.colors.length > 0) {
    //   this.clearColor(1);
    // }
    // if (this.product.sizes.length > 0) {
    //   this.clearSize(1);
    // }
  }

  ionViewWillLeave() {
    // this.tabBarElement.style.display = 'flex';
  }

  ionViewDidLoad() {
    this.getDetailsAll();
    console.log('ionViewDidLoad ProductPage');
    // this.tabBarElement.style.display = 'none';
  }

  // clearColor(pos) {
  //   for (var i = 0; i < this.cb.length; i++) {
  //     if (i !== pos) {
  //       this.cb[i] = false;
  //     }
  //   }
  //   setTimeout(() => {
  //     this.cb[pos] = true;
  //     this.currentColor = this.product.colors[pos];
  //   }, 200);
  // }
  //
  // clearSize(pos) {
  //   for (var i = 0; i < this.size.length; i++) {
  //     if (i !== pos) {
  //       this.size[i] = false;
  //     }
  //   }
  //   this.size[pos] = true;
  //   this.currentSize = this.product.sizes[pos];
  // }
  //
  selectQty() {
    this.qtySelect.open();
  }
  //
  loveIt() {
    this.love = !this.love;
  }
  //
  // quantityChange() {
  //   console.log(this.quantity);
  //   this.currentQty = 'تعداد: ' + this.quantity.toString();
  // }
  //
  goCart() {
    // this.navCtrl.pop();
    // setTimeout(() => {
    //   this.navCtrl.parent.select(2);
    // }, 100);
    this.navCtrl.setRoot('MyCartPage')
  }

  // add2Cart() {
  //   let flgFound = false;
  //   this.cart.products.forEach(item => {
  //     if (item.product.id === this.product.id) {
  //       flgFound = true;
  //       item.quantity = parseInt(item.quantity.toString()) + parseInt(this.quantity.toString());
  //     }
  //   })
  //   if (!flgFound) {
  //     this.cart.products.push({ product: this.product, quantity: this.quantity, color: this.currentColor, size: this.currentSize });
  //   }
  //   setTimeout(() => {
  //     this.navCtrl.pop();
  //   }, 300);
  // }



  addToBuy(){
    // console.log("selectedColors");
    //             console.log(this.product.selectedColor);
    let empty = false;
    // if(localStorage["products"]){
      if(this.cartService.productsOfCart$.length < 1){
        empty = true;
      }
    // }
    if(this.cartService.productsOfCart$ == null || empty){
      // let products:Product[]=[];
      //this.product.numberInCart += 1;
      console.log(typeof this.product.selectedColor.numberInCart);
      this.product.selectedColor.numberInCart = this.numberInCart;
      this.cartService.productsOfCart$.push(Object.assign({},this.product));
      this.cartService.emit(true);
      this.setCart();
      console.log(typeof this.product.selectedColor.numberInCart);
      // products.push(this.product);
      // localStorage["products"] = JSON.stringify(products);
    }
    else{
      // let products = JSON.parse(localStorage["products"]);
      let len = this.cartService.productsOfCart$.length;
      let exist = false;
      console.log('color')
      console.log( this.cartService.productsOfCart$);
      console.log(this.product.selectedColor);
      this.cartService.productsOfCart$.forEach((product,i)=>{
        if(product.id == this.product.id && product.selectedColor.id == this.product.selectedColor.id){
          console.log("number in cart ------------")
          console.log(typeof product.selectedColor.numberInCart);
          console.log( Number(product.selectedColor.numberInCart));
          console.log(Number(this.numberInCart));
          // let num = ;
         let num = Number(product.selectedColor.numberInCart) + Number(this.numberInCart);
         console.log('num = '+num);
          product.selectedColor.numberInCart = num ;
          exist = true;
          this.setCart();

        }
        if(len - 1 == i && !exist){
          this.product.selectedColor.numberInCart = this.numberInCart;
          this.cartService.productsOfCart$.push(Object.assign({},this.product));
          this.cartService.emit(true);
          this.setCart();
        }
      });

      // products.push(this.product);
      // localStorage["products"] = JSON.stringify(products);
    }
    console.log('products cart ------------------');
    console.log(this.cartService.productsOfCart$);
    // console.log(localStorage['products']);
    // let change:boolean = true;
    // this.addProductEvent.emit(change);
   // this.notificationsService.emit(true);
    alert("به سبد خرید شما اضافه شد");
    // console.log("details");
    // localStorage["mycars"] = JSON.stringify(mycars);

    // var cars = JSON.parse(localStorage["mycars"]);
  }
  getId(value:any){
    return value;
  }
  // getDetails(){
  //   this.brandService.getOne(this.urlProduct,this.product_id).then(res=>{
  //     this.product=res;
  //     this.product.brand = res.brand;
  //     // this.review.nativeElement.innerHTML = this.product.review_text;
  //     //  this.brandService.getOne(this.brandUrl,res.brand_id).then((brand)=>{
  //     //    this.product.brand=brand;
  //      console.log(res);
  //     //  });
  //     this.product.colors[0].selected = true;
  //     this.product.colors.forEach((color,i)=>{
  //       if(i == 0){
  //         color.selected = true;
  //       }
  //       else{
  //         color.selected = false;
  //       }
  //     });
  //     this.product.selectedColor = this.product.colors[0];
  //     this.price = this.product.colors[0].price;
  //     if(parseInt(this.product.colors[0].number)>0){
  //       this.exist = "موجود";
  //     }
  //     else{
  //       this.exist = "ناموجود";
  //     }
  //     // product.colors.forEach((color,i) => {
  //     //   if(i == 0) {
  //     //     this.cb.push(true);
  //     //   }
  //     //   else {
  //     //     this.cb.push(false);
  //     //   }
  //     // });
  //     // console.log(this.cb);
  //   });
  //
  //
  //
  //
  //   // this.brandService.getOne(this.productcategoryUrl,this.product_id).then((cats)=>{
  //   //   this.product.categories= cats;
  //   //   //  console.log(cats);
  //   // });
  //
  //   this.brandService.getOne(this.attributeUrl,this.product_id).then((attribues)=>{
  //     this.product.attributes=attribues;
  //     // console.log(attribues);
  //   });
  //   this.brandService.getOne(this.importantAttributeUrl,this.product_id).then((attribues)=>{
  //     this.product.importantAttributes=attribues;
  //     console.log("important______________________")
  //     console.log(attribues);
  //   });
  //   this.brandService.getOne(this.plusViewUrl,this.product_id).then((pr)=>{
  //     console.log("plus");
  //     console.log(pr);
  //   });
  //   // this.brandService.getOne(this.navbarUrl,this.product_id).then((cats)=>{
  //   //   this.navbars = cats;
  //   //   console.log("cats")
  //   //   console.log(cats)
  //   // });
  //   // this.brandService.getOne(this.avgPointUrl,this.product_id).then((average)=>{
  //   //   this.setRating(this.product,average);
  //   //   // this.product.halfRate = Math.ceil(average - this.product.completeRate);
  //   //   // this.product.emptyRate = 5 - this.product.completeRate -this.product.halfRate;
  //   //
  //   //   console.log("average")
  //   //   console.log(average);
  //   // });
  //
  //
  // }



  selectColor(i:number,event:any){
    // let classNames = event.path[2].getElementsByClassName('checkbox-icon')[0].className ;
    // console.log(event.path[2].getElementsByClassName('checkbox-icon')[0].className);
    // classNames += ' ' + 'checkbox-checked';
    // console.log(event);
    setTimeout(() => {
      event.path[2].getElementsByClassName('checkbox-icon')[0].classList.add('checkbox-checked');
      this.product.colors[i].selected = true;
      // if(this.product.colors[i].selected == true) {
        let exist = false;
        this.product.colors.forEach((color,j)=>{
          // color.selected = false;
          if(color.selected == true && i !== j){
            color.selected = false;
          }
        });
        this.product.selectedColor = Object.assign({},this.product.colors[i]);
        this.price = this.product.colors[i].price ;
        // this.product.selectedColor.numberInCart = 0;
        if(parseInt(this.product.colors[i].number)>0){
          this.exist = "موجود";
        }
        else{
          this.exist = "ناموجود";
        }
      // }
      // else {
      //   this.product.selectedColor == null;
      // }
      console.log(this.product.selectedColor);
    },20);
    // setTimeout(() => {
    //   event.target.getElementsByClassName('checkbox-icon')[0].classList.add('checkbox-checked');
    // },20);

    // console.log(event.path[2].getElementsByClassName('checkbox-icon')[0].className);
    // let existClass = classNames.includes('checkbox-checked');
    // if(existClass) {
    //   classNames += ' ' + 'checkbox-checked';
    //   event.path[2].getElementsByClassName('checkbox-icon')[0].className = classNames;
    //   console.log(existClass);
    //   console.log(event.path[2].getElementsByClassName('checkbox-icon')[0].className)
    // }
   // console.log();

    // let exist = false;
    // this.product.colors.forEach((color,j)=>{
    //   // color.selected = false;
    // });
    // this.product.colors[i].selected = true;

  }
  setNumberInCart(number) {
    this.numberInCart = number;
  }
  // plusNumber(){
  //   this.numberProduct += 1;
  //   // this.product.selectedColor.numberInCart = this.
  // }
  // minusNumber(){
  //   if(this.numberProduct > 1){
  //     this.numberProduct -= 1;
  //   }
  // }
  // changePoint(input:any){
  //   console.log("change-----------------")
  //   console.log(input.target.value);
  // }
  // isLogin(){
  //   this.authService.isLogin();
  //   this.islogin = this.authService.isLogin$;
  //   //return this.islogin;
  // }
  //
  // barrating(){
  //   let authService = this.authService;
  //   let router = this.router;
  //   let user_id = 0;
  //   let product_id = this.product_id;
  //   let url = this.pointUrl;
  //   let sendPoint = this.sendPoint;
  //   let api = this.api;
  //   // let islogin = false;
  //   jQuery('#example-css').barrating('show', {
  //     theme: 'fontawesome-stars',
  //     onSelect: function(value, text, event) {
  //       if (typeof(event) !== 'undefined') {
  //         if(authService.isLogin()){
  //           console.log("login hastid")
  //           user_id = JSON.parse(localStorage.getItem('user')).id;
  //           console.log("user_id"+"="+user_id);
  //           sendPoint(api,product_id,user_id,parseInt(value),url);
  //         }
  //         else{
  //           alert("برای امتیاز دادن لطفا لاگین کنید");
  //           router.navigate(['/signin']);
  //         }
  //         console.log("ratting------------------")
  //         console.log(value);
  //       } else {
  //         // rating was selected programmatically
  //         // by calling `set` method
  //       }
  //     }
  //   });
  //
  //   //      $(function() {
  //   //     $('#example').barrating({
  //   //       theme: 'fontawesome-stars'
  //   //     });
  //   //  });
  // }
  // sendPoint(api:BrandServiceToken,product_id:number,user_id:number,point:number,url:string){
  //   let formData = new FormData();
  //   formData.append('product_id',product_id.toString());
  //   formData.append('user_id',user_id.toString());
  //   formData.append('point',point.toString());
  //   api.create(url,formData).then((res)=>{
  //     alert("عملیات شما با موفقیت انجام شد")
  //     console.log("point--------------");
  //     console.log(res);
  //   });
  // }
  // setRating(product:Product,average:number){
  //   product.completeRate = [];
  //   product.halfRate = [];
  //   product.emptyRate = [];
  //   let completeRate = Math.floor(average);
  //   console.log("completerate "+ completeRate);
  //   while(completeRate > 0){
  //     product.completeRate.push(5);
  //     completeRate -= 1 ;
  //   }
  //
  //   let halfRate = Math.ceil(average - Math.floor(average));
  //   console.log("halfrate "+ halfRate);
  //   while(halfRate > 0){
  //     product.halfRate.push(5);
  //     halfRate -= 1 ;
  //   }
  //
  //   let emptyRate = 5 - Math.floor(average) - Math.ceil(average - Math.floor(average));
  //   console.log("empryrate "+ emptyRate);
  //   while(emptyRate > 0){
  //     product.emptyRate.push(5);
  //     emptyRate -= 1 ;
  //   }
  // }
  // setDefaultBarrating(){
  //   if(this.authService.isLogin()){
  //     let formData = new FormData();
  //     let user_id = JSON.parse(localStorage.getItem('user')).id;
  //     formData.append('user_id',user_id);
  //     formData.append('product_id',this.product_id.toString());
  //     this.api.create(this.pointUserUrl,formData).then((res)=>{
  //       jQuery('select').barrating('set', res.point);
  //       console.log("get poin user")
  //       console.log(res.point)
  //     })
  //   }
  // }

  // sendComment(form:NgForm,message:any){
  //   if(this.authService.isLogin()){
  //     let user_id = JSON.parse(localStorage.getItem('user')).id;
  //     let formData = new FormData();
  //     // console.log("message")
  //     // console.log(this.message);
  //     formData.append('message',message);
  //     formData.append('user_id',user_id);
  //     formData.append('product_id',this.product_id.toString());
  //     this.api.create(this.commentUrl,formData).then((comment)=>{
  //       console.log("comment")
  //       console.log(comment);
  //       form.reset();
  //       alert("عملیات شما با موفقیت انجام شد");
  //     })
  //   }
  //   else{
  //     alert("برای ارسال نظر لطفا لاگین کنید");
  //     this.router.navigate(['/signin']);
  //   }
  // }
  setStylePrice(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }
  changeNumber(){
    console.log('number in cart -------');
    console.log(this.numberInCart);
  }
  setCart() {
    this.allCartNumber = 0;
    let len = this.cartService.productsOfCart$.length;
    this.cartService.productsOfCart$.forEach((pr,i) => {
      let num = Number(this.allCartNumber) + Number(pr.selectedColor.numberInCart);
      console.log('cart --------------');
      console.log('all: ' + this.allCartNumber);
      console.log( 'slect: '+pr.selectedColor.numberInCart);
      console.log('num: '+num);
      this.allCartNumber = Number(this.allCartNumber) + Number(pr.selectedColor.numberInCart);
      if(i == len-1){
        this.cartService.emit(true);
      }
    });
  }

  getDetailsAll() {
    // this.loading = true ;
    this.brandService.getOneObservable(this.navbarUrl, this.product_id).subscribe((cats)=>{
      this.navbars = cats;

    });
    this.brandService.getOneObservable(this.urlProductAllDataGroup, this.product_id).subscribe(products=>{
      console.log('res-------------------------------');
      console.log(products);
      this.products = products;
      // this.categories = products[0].categories;
      // console.log(this.products[0])
      //----------------------------------------------------------------------------------------
      this.productSecond = products[0];
      this.productSecond.comments = products[0].comments;
      console.log("coments------------");
      console.log(this.productSecond.comments);
      // console.log()
      this.productSecond.importantAttributes = products[0].importantAttributes;
      this.productSecond.categories = products[0].categories;
      this.productSecond.attributes = products[0].attributes;
      this.productSecond.attributesCheck = products[0].attributesCheck;
      this.product = this.productSecond;
      // console.log("review text")
      // console.log(this.product.review_text)
      // this.review.nativeElement.innerHTML = this.product.review_text;

      this.product.colors[0].selected = true;
      this.product.colors.forEach((color,i) => {
        if(i == 0){
          color.selected = true;
        }
        else{
          color.selected = false;
        }
      });
      this.product.selectedColor = this.product.colors[0];
      this.price = this.product.colors[0].price;

      if( parseInt (this.product.colors[0].number) > 0 ) {
        this.exist = "موجود";
      }
      else {
        this.exist = "ناموجود";
      }
      // this.loading = false ;
      console.log('atttributes----')
      console.log(this.product.attributesCheck)
      // this.product.attributes = [];
      this.products.forEach((pr,i) => {
        pr.attributesCheck.forEach((attr,j) => {
          if (attr.description.trim().length == 0) {
            pr.attributesCheck.splice(j,1);
          }
        });
        this.users.push(pr.user);
      });
      this.product_id = this.product.id;
      // this.barrating();
      // this.setRating(this.product, this.product.averagePoint);
      // this.setDefaultBarrating();
    });

    this.brandService.getOneObservable(this.plusViewUrl, this.product_id).subscribe((pr) => {
    });




  }
  changeUser(event) {
    console.log('change shopping')
    console.log(event);
    let id = event;
    this.product = this.products.filter(pr => pr.id == id)[0];
    // this.review.nativeElement.innerHTML = this.product.review_text;

    this.product.colors[0].selected = true;
    this.product.colors.forEach((color,i) => {
      if(i == 0) {
        color.selected = true;
      }
      else {
        color.selected = false;
      }
    });
    this.product.selectedColor = this.product.colors[0];
    this.price = this.product.colors[0].price;

    if( parseInt (this.product.colors[0].number) > 0 ) {
      this.exist = "موجود";
    }
    else {
      this.exist = "ناموجود";
    }
    this.product_id = this.product.id;
    // this.barrating();
    // this.setDefaultBarrating();
    // this.setRating(this.product, this.product.averagePoint);
  }
}
