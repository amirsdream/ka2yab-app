import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Cart } from '../../providers/database';


import {baseUrl} from '../../baseurl';
import {BrandService} from '../../brand.service';
import {BrandServiceToken} from '../../servicetoken.service';
import {Product} from '../../product';
import {Image} from '../../image';
import {Color} from '../../color';
import {Category1} from '../../category';
import { LoadingController } from 'ionic-angular';
import {NotificationsService} from '../../addproduct.service';
import {ShowdetailsService} from '../../showdetails.service';
import {AuthenticatinService} from '../../authentication.service';
import {CartService} from '../../cart.service';
import {Service} from '../../service.service';
import {MegamenuService} from '../../megamenu.service';
import {NgForm} from "@angular/forms";

/**
 * Generated class for the Tabs tabs.
 *
 * See https://angular.io/docs/ts/latest/guide/dependency-injection.html for
 * more info on providers and Angular DI.
 */
@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root: string = 'HomePage';
  tab2Root: string = 'GoodsellPage';
  tab3Root: string = 'MyCartPage';
  tab4Root: string = 'SearchPage';
  tab5Root: string = 'ProfilePage';

  detail: NavParams;
  selectedIndex: number;
  cart: Cart;
  allCartNumber = 0;

  constructor(public navCtrl: NavController, private params: NavParams, private cartService: CartService) {
    this.selectedIndex = params.data.tabIndex || 0;
    this.detail = params;
    this.cart = Cart.getInstance();
    this.cartService.emitChange.subscribe(()=>{
      this.setCart();
    });
    // this.navCtrl.push('CategoriesPage', {menus: parent, select: detail});
  }
  setCart() {
    this.allCartNumber = 0;
    this.cartService.productsOfCart$.forEach((pr,i) => {
      let num = Number(this.allCartNumber) + Number(pr.selectedColor.numberInCart);
      console.log('cart --------------');
      console.log('all: ' + this.allCartNumber);
      console.log( 'slect: '+pr.selectedColor.numberInCart);
      console.log('num: '+num);
      this.allCartNumber = Number(this.allCartNumber) + Number(pr.selectedColor.numberInCart);
    });
  }
}
