import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Category, Database } from '../../providers/database';
import {BrandService} from '../../brand.service' ;
import { Slider } from "../../slider";
import {baseUrl} from '../../baseurl';
import {AlldataService} from '../../alldata.service' ;
import {Product} from '../../product';



/**
 * Generated class for the HotOffer page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-hotoffers',
  templateUrl: 'hotoffers.html',
})
export class HotoffersPage {

  goodSellProducts:Product[]=[];
  goodSellProductsSecond:Product[]=[];
  imageUrl = baseUrl +"api/images";
  imageDir = "images/";
  baseUrl = baseUrl

  menuItems: Category[];

  constructor(public navCtrl: NavController, public navParams: NavParams,private api:BrandService,private data:AlldataService,) {
    let db = Database.getInstance();
    this.menuItems = db.parentCategory();

    this.data.emitChangeBlog.subscribe(text=>{
      if(typeof text == "boolean"){
        this.setData();
      }
      // this.setBlogs();
    }) ;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HotoffersPage');
  }

  categories(id: string) {
    this.menuItems.forEach(item => {
      if(item.id === id) {
        this.navCtrl.push('CategoriesPage', {menus: item, select: item.children[0].name.toLowerCase()});
      }
    })
  }
  setData(){
    // console.log(this.recentlyProducts);
    // this.goodCategories = this.data.goodCategories$;
    // console.log("set data ctegory")
    // console.log(this.goodCategories)
    // this.beforeRecentlyProducts = this.data.recentlyProducts$;
    // this.beforeRecentlyProductsSecond = this.data.recentlyProducts$;
    this.goodSellProducts = this.data.specailSellProducts$;
    this.goodSellProductsSecond = this.data.popularProducts$;
    console.log("goodsell---------------") ;
    console.log(this.goodSellProductsSecond) ;
    // console.log('recently ---------------')
    // console.log(this.beforeRecentlyProductsSecond);
    // this.brands = this.data.brands$;
    // console.log("brands all data")
    // console.log(this.brands);

  }

}
