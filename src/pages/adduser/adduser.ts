import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the Signin page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-adduser',
  templateUrl: 'adduser.html',
})
export class AdduserPage {
  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad SigninPage');
  }

  home() {
    this.navCtrl.setRoot('TabsPage');
  }

  register() {
    this.navCtrl.setRoot('SignupPage');
  }
  signup(page){
    this.navCtrl.setRoot(page);
  }
}
