import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddagentPage } from './addagent';

@NgModule({
  declarations: [
    AddagentPage,
  ],
  imports: [
    IonicPageModule.forChild(AddagentPage),
  ],
  exports: [
    AddagentPage
  ]
})
export class SignupModule {}
