import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {NgForm} from '@angular/forms';
import { LoadingController } from 'ionic-angular';
import { Address } from '../../iran-address';
import {baseUrl} from '../../baseurl';
import {User} from '../../user';
import {BrandService} from '../../brand.service';

/**
 * Generated class for the Signup page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-addagent',
  templateUrl: 'addagent.html',
})
export class AddagentPage {

  resultCaptcha = false;
  error = "";
  tokenCaptcha = null ;
  urlExistEmail=baseUrl+'api/exist/email/home';
  agentCityUrl:string=baseUrl+'api/users/marketer/city/home';
  urlUser:string=baseUrl+'api/users/home';
  // email = "" ;
  existEmail  = false ;
  samePassword = false ;

  address = new Address();
  users:User[] = [];
  selectedUsers:User[] = [];
  numbers:number[] = [];
  states = [];
  cities = [];
  city:string="";
  province:string="";
  stateChange = false ;
  stateEmpty = false ;
  cityEmpty = false ;
  cityChange = false ;
  loading = false ;

  constructor(private brandService:BrandService, public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController) {
    this.states = this.address.states ;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }

  skip() {
    this.navCtrl.setRoot('TabsPage');
  }

  register() {
    this.navCtrl.setRoot('TabsPage');
  }


  changeState(input:any){
    // console.log(input)
    this.stateChange = true ;
    if(input == '') {
      this.stateEmpty = true ;
    }
    else {
      this.stateEmpty = false ;
    }
    console.log(input)
    this.cities = this.address.loadCity(input);
    // alert("salamsdfdfadweddd")
    // this.city = this.cities[0];
    console.log(this.city + this.province)
    this.getMarketer();
  }
  changeCity(input:any) {
    this.cityChange = true ;
    if ( input === '' ) {
      this.cityEmpty = true ;
    }
    else {
      this.cityEmpty = false ;
    }
    console.log(input);
    // this.cities = this.address.loadCity(input.target.value)
    // alert("salamsdfdfadweddd")
    this.city = input;
    console.log(this.city + this.province)
    this.getMarketer();
  }
  checkSamePassword(password, passRepeat) {
    setTimeout(() => {
      console.log(password.target.value + '=' + passRepeat) ;
      if(password.target.value != passRepeat && password.target.value != "" && passRepeat != "") {
        this.samePassword = true ;
      }
      else {
        this.samePassword = false ;
      }
    },200);

  }
  checkSamePasswordRepeat(password, passRepeat) {
    setTimeout(() => {
      console.log(password+ '=' + passRepeat.target.value) ;
      if(password != passRepeat.target.value && password != "" && passRepeat.target.value != "") {
        this.samePassword = true ;
      }
      else {
        this.samePassword = false ;
      }
      console.log(this.samePassword);
    },200);

  }
  emailKeyDown(event: any ) {
    this.existEmail = false ;
  }
  upload(f , firstName,lastName,email,pass,repeatPas,phone,mobile,address,zip_code){
    this.error = "";
    const loading = this.loadingCtrl.create({
      content: `
      <div class="custom-spinner-container  ">
        <div class="custom-spinner-box  "></div>
      </div>`,
    });

    console.log("form send")
    const formData: any = new FormData();
    // console.log(f.controls.email.dirty) ;
    console.log(f.valid) ;
    // const files: Array<File> = this.filesToUpload;
    // formData.append("file", files[0], files[0]['name']);



    //
    if(pass != repeatPas && pass != "" && repeatPas != ""){
      this.error  +=  "پسورد و تکرار پسورد یکسان نیستند.\n";
    }

    if(this.province == '') {
      this.stateEmpty = true ;
      this.error  +=  "فیلد استان خالی می باشد.\n";

    }
    else {
      this.stateEmpty = false ;
    }
    if(this.city == '') {
      this.cityEmpty = true ;
      this.error  +=  "فیلد شهر خالی می باشد.\n";
    }
    else {
      this.cityEmpty = false ;
    }
    console.log( this.province + this.city + this.cityEmpty + this.stateEmpty)
    // if(firstName == ""){
    //     this.error  +=  "فیلد نام خالی می باشد.\n";
    // }
    // if(lastName == ""){
    //     this.error  +=  "فیلد نام خانوادگی خالی می باشد.\n";
    // }
    // if(email == ""){
    //     this.error  +=  "فیلد ایمیل خالی می باشد.\n";
    // }
    // if( pass == ""){
    //     this.error  +=  "فیلد پسورد خالی می باشد.\n";
    // }
    // if(mobile == ""){
    //     this.error  +=  "فیلد موبایل خالی می باشد.\n";
    // }
    // if(phone == ""){
    //     this.error  +=  "فیلد تلفن خالی می باشد.\n";
    // }
    //
    // if(address == ""){
    //     this.error  +=  "فیلد آدرس خالی می باشد.\n";
    // }
    // if(zip_code == ""){
    //     this.error  +=  "فیلد کدپستی خالی می باشد.\n";
    // }
    if( email != '' ) {
      let formDataEmail = new FormData();
      formDataEmail.append('email', email);
      this.loading = true ;
      loading.present();
      this.brandService.createObservable(this.urlExistEmail, formDataEmail).subscribe((res) => {
        //alert(res.exist)
        if (res.exist == true) {
          this.existEmail = true ;
          this.error = "این ایمیل قبلا ثبت شده است.\n";
          // alert(this.error);
          // this.error = "";
          loading.dismiss();
          this.loading = false ;
        }
        else {
          // console.log("token captcha === " + this.resultCaptcha) ;
          // console.log("length token " + this.captcha.getResponse().length ) ;
          if (this.error == ""  && f.valid && !this.stateEmpty && !this.cityEmpty) {
            // console.log( "send token == " + this.captcha.getResponse() ) ;
            formData.append("firstName", firstName);
            formData.append("lastName", lastName);
            formData.append("email", email);
            formData.append("password", pass);
            formData.append("mobile", mobile);
            formData.append("role", "agent");
            formData.append("active",false);
            formData.append("phone", phone);
            formData.append("address", address);
            formData.append("zip_code", zip_code);
            formData.append("state", this.province);
            formData.append("city", this.city);
            this.brandService.createObservable(this.urlUser, formData).subscribe(
              res => {
                // alert("عملیات شما با موفقیت انجام شد");
                this.loading = false ;
                // this.router.navigate(['/success']);
                this.navCtrl.setRoot('TabsPage');
                loading.dismiss();
                // alert("Success") ;
                console.log("success") ;
              });
          }
          else {
            // alert(this.error);
            console.log("empty city");
            this.error = "";
            this.loading = false ;
            loading.dismiss();
            //console.log( this.captcha.getResponse() ) ;
          }
        }
      });
    }
    else {
      //alert(this.error);
      //this.error = '';
      console.log('email empty')
    }
  }
  add(){
    // let value = new Filtervalue();
    this.numbers.push(0);
  }
  delete(i:number){
    if( this.numbers.length >1 ){
      this.numbers.splice(i,1);
      this.selectedUsers.splice(i,1);
      console.log(this.selectedUsers)
    }
  }
  getMarketer(){
    const loading = this.loadingCtrl.create({
      content: `
      <div class="custom-spinner-container  ">
        <div class="custom-spinner-box  "></div>
      </div>`,
    });

    let formData = new FormData();
    formData.append("state",this.province);
    formData.append("city",this.city);
    // alert(this.city+this.province)
    loading.present();
    this.brandService.createObservable(this.agentCityUrl, formData ).subscribe (
      res => {
        this.users = res;
        loading.dismiss();
        // this.selectedUsers = [];
        // if(res.length>0){
        //   this.selectedUsers.push(res[])
        // }
        // console.log(res)
        // alert("عملیات شما با موفقیت انجام شد");
        // this.router.navigate(['/dashboard/user/show']);
      });
  }
  changeMarketer( ids:any ){
    this.selectedUsers = [];
    ids.forEach((id,i) => {
      let user = this.users.filter(user=>user.id == id)[0];
      if(user){
        this.selectedUsers.push(Object.assign({}, user));
      }
    });
    console.log('selected users');
    console.log(this.selectedUsers);
    // if(input != 0){
    //   this.selectedUsers[i]=this.users.filter(user=>user.id == input)[0];
    // }
    // else{
    //   this.selectedUsers.splice(i,1);
    // }

    // console.log(input.target.value)
    // console.log(this.selectedUsers)
  }
  test(input:any){
    console.log('input mulriselect');
    console.log(input);
  }

}
