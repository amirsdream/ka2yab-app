import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { Product, Database } from '../../providers/database';
import {Filtertitle} from '../../filtertitle';
import {Categoryattribute} from "../../categoryattribute";
import {Attribute} from '../../attribute';
import {Pricerange} from '../../pricerange';
import {Brand} from '../../brand';

/**
 * Generated class for the Filtermodal page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-filtermodal',
  templateUrl: 'filtermodal.html',
})
export class FilterModalPage {
  products: Product[];
  filterTypes: any;
  selectedFilter: any;

  structure: any = { lower: 33, upper: 60 };
  filtertitles:Filtertitle[] = [];
  selectedId = 1;
  categoryattributes:Categoryattribute[] = [];
  categoryattributesAll:Categoryattribute[] = [];
  secondCategoryattributes:Categoryattribute[] = [];
  selectedAttribute:Attribute[] = [];
  selectedCategoryattributes:Categoryattribute[] = [];
  // range: any = { min: 0, max: 100 };
  range = new Pricerange();
  selectedRange = new Pricerange();
  brands:Brand[] = [];
  selectedBrands:Brand[] = [];
  order_id:number = 2;

  constructor(public navCtrl: NavController, public navParams: NavParams, private view: ViewController) {
    this.setFilterTitle();
    this.products = new Array<Product>();
    this.filterTypes = new Array<any>();
    var db = Database.getInstance();
    this.products = db.allProduct();
    this.filterTypes = db.allFilters();
    this.selectedFilter = this.filterTypes[0];
    this.order_id = parseInt(this.navParams.get('order_id'));
    console.log(this.order_id);
    this.selectedCategoryattributes = this.navParams.get('selectedCategoryattributes')

    let categoryattributes = this.navParams.get('categoryattributes');
    categoryattributes.forEach((catAttr,i) => {
      catAttr.selectedAttrs = [];
      catAttr.selectedAttributes = [];

      if(this.selectedCategoryattributes.length > 0) {
        let existCatAttr = this.selectedCategoryattributes.filter(selectedAttr => selectedAttr.id == catAttr.id);
        if (existCatAttr.length > 0) {
          catAttr.selectedAttributes = existCatAttr[0].selectedAttributes;
          catAttr.selectedAttrs = existCatAttr[0].selectedAttrs;
        }
      }
      else {
        this.categoryattributesAll.push(Object.assign({},catAttr));
      }
      this.categoryattributes.push(Object.assign({},catAttr));
      // this.selectedCategoryattributes.push(Object.assign({},catAttr));
      if(i == categoryattributes.length - 1) {
        console.log('------------catsattrs--------');
        console.log(this.categoryattributes);
      }
    });


    this.selectedBrands =  this.navParams.get('selectedBrands');
    console.log('selected brs')
    console.log(this.selectedBrands)
    let brands =  this.navParams.get('brands');
    // this.brands = brands;
    this.range = this.navParams.get('range');
    this.selectedRange = this.navParams.get('selectedRange');
    brands.forEach(br=>{
      // br.selected = false;
      if(this.selectedBrands.length > 0) {
        let existBr = this.selectedBrands.filter(selectedBr=>br.id == selectedBr.id );
        if(existBr.length > 0) {
          br.selected = true;
          console.log('true shode')
        }
      }
      this.brands.push(Object.assign({},br));
    });


    // this.brands.forEach(br=>{
    //   br.selected = false;
    // });
    // this.range = Object.assign({},this.range);
    this.structure.lower = this.selectedRange.min;
    this.structure.upper = this.selectedRange.max;
    console.log('brands-------------------')
    console.log(this.brands);
    console.log(this.selectedBrands);
    console.log('---------------------------------');
    console.log(this.selectedRange);
    // console.log(this.navParams.get('categoryattributes'));
  }
  setFilterTitle(){
    let price = new Filtertitle();
    price.id = 1;
    price.name = 'رنج قیمت';
    price.selected = true;
    this.filtertitles.push(price);

    let price1 = new Filtertitle();
    price1.id = 2;
    price1.name = 'مرتب سازی';
    price1.selected = false;
    this.filtertitles.push(price1);

    let price2 = new Filtertitle();
    price2.id = 3;
    price2.name = 'برند';
    price2.selected = false;
    this.filtertitles.push(price2);

    let price3 = new Filtertitle();
    price3.id = 4;
    price3.name = 'فیلترها';
    price3.selected = false;
    this.filtertitles.push(price3);

  }
  selectOrder(event) {
    this.order_id = parseInt(event);
    console.log(this.order_id)
  }
  selectBrand(selectedBrandsId,index) {
    this.selectedBrands = [];
    this.brands.forEach(br=>{
      br.selected = false;
    })
    selectedBrandsId.forEach((id,i)=>{
      let brand = this.brands.filter(br => br.id == id)[0];
      brand.selected = true;
      this.selectedBrands.push(Object.assign({},brand));
    });
    console.log('brand selected------------------')
    console.log(this.selectedBrands)
    console.log(this.brands)
    console.log('-------------------------------------------')
  }
  selectedCats(event,i) {
    // this.selectedCategoryattributes[i].
    this.categoryattributes[i].selectedAttrs = event;
    this.categoryattributes[i].selectedAttributes = [];
    this.categoryattributes[i].attributes.forEach((attr)=> {
      attr.selected = false;
    });
    this.categoryattributes[i].attributes.forEach((attr)=> {
      event.forEach((attrId,j)=>{
        if(attrId == attr.id){
          attr.selected = true;
          this.categoryattributes[i].selectedAttributes.push(Object.assign({},attr));
        }
      });
    });
    console.log('selected');
    console.log( this.categoryattributes[i])
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad FiltermodalPage');
    this.filterTypes.forEach(item => {
      if (item.selected) {
        this.selectedFilter = item;
      }
    })
  }

  chooseTab(filterTitle,id) {
    this.filtertitles.forEach(item => {
      item.selected = false;
    });
    filterTitle.selected = true;
    this.selectedId = id;
    // this.selectedFilter = filterTitle;
  }

  selectFilters(selectedFilter, val) {
    if (selectedFilter.type === 'or') {
      selectedFilter.filters.forEach(f => {
        if (f !== val) {
          f.checked = false;
        }
      })
      val.checked = !val.checked;
    } else {
      val.checked = !val.checked;
    }
  }

  clearAll() {
    let currentId = this.selectedId;

    console.log('current = '+currentId);
    this.selectedId = 5;
    console.log('change = '+this.selectedId);
    setTimeout(()=>{
      this.structure.upper = this.range.max;
      this.structure.lower = this.range.min;
      // this.categoryattributes = [];
      this.selectedCategoryattributes = [];
      this.selectedBrands = [];
      this.categoryattributes.forEach(catAttr=>{
        catAttr.attributes.forEach(attr=>{
          if (attr.selected == true) {
            attr.selected = false;
          }
        })
        catAttr.selectedAttrs = [];
        catAttr.selectedAttributes = [];
      });
      console.log('clear attributes------')
      console.log(this.categoryattributes)
      this.brands.forEach(br=>{
        br.selected = false;
      });
      this.order_id = 2;
      this.selectedId = currentId;
      console.log('after = '+this.selectedId);

    },20)

  }

  applyFilter() {
    this.selectedCategoryattributes = [];
    this.categoryattributes.forEach((catAttr,i) => {
      if ( catAttr.selectedAttributes.length > 0 ) {
        catAttr.attributes = catAttr.selectedAttributes;

      }
  // else {
  //   this.categoryattributes.splice(i,1);
  // }
      if(i == this.categoryattributes.length){

      }
    });
    this.selectedCategoryattributes = this.categoryattributes.filter(catAttr => catAttr.selectedAttributes.length > 0);


    // console.log('cat attr aply ------------------')
    // console.log(this.selectedCategoryattributes);

    this.dismiss();
  }

  dismiss() {
    var data = {
      categoryattributes: this.selectedCategoryattributes,
      brands: this.selectedBrands,
      order_id:this.order_id,
      structure: this.structure,
    }
    // Returning data from the modal:
    this.view.dismiss(
      data
    );
  }

  close() {
    this.navCtrl.pop();
  }
}
