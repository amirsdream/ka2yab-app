import { Component, ViewChild } from '@angular/core';

import { Platform, MenuController,NavController, NavParams, Nav ,IonicPage} from 'ionic-angular';
// import { IonicPage, NavController, NavParams, Slides } from 'ionic-angular';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { Database, Cart, Category } from '../providers/database';
import {AlldataService} from '../alldata.service' ;
import {BrandService} from '../brand.service' ;
import {baseUrl} from '../baseurl' ;
import {Menu} from '../menu';
import { Storage } from '@ionic/storage';
import {AuthenticatinService} from '../authentication.service';
import {User} from '../user';
// import {Category1} from '../category';

declare const ENV;

export interface PageInterface {
  title: string;
  name: string;
  icon?: string;
  logsOut?: boolean;
  index?: number;
  tabName?: string;
  detail?: string;
}

@Component({
  templateUrl: 'app.html'
})
export class Ecom9App {

  menues:Menu[] = [];
  secondMenues:Menu[] = [];
  categoryMenuUrl = baseUrl +"api/category/parents";
  categorySubMenuUrl = baseUrl +"api/category/subs/all";
  homeDataUrl = baseUrl +"api/home/data";
  user = new User();
  isLogin = false;
  @ViewChild(Nav) nav: Nav;
  database: Database;
  cart: Cart;
  menuItems: Category[];
  // make HelloIonicPage the root (or first) page
  rootPage: string;
  pages: PageInterface[] = [
    { title: 'Track Orders', name: 'TabsPage', tabName: 'پروفایل', index: 4, detail: 'my orders' },
    { title: 'Account details', name: 'TabsPage', tabName: 'پروفایل', index: 4, detail: 'profile' },
    { title: 'Sign out', name: 'SigninPage', tabName: 'ورود', index: 2 },
  ];

  constructor(
    public platform: Platform,
    public menu: MenuController,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    private data:AlldataService ,
    private api:BrandService,
    private storage:Storage,
    private authService: AuthenticatinService
  ) {
    this.rootPage = 'TabsPage';
   // this.storage.remove('mahmoud');
    this.initializeApp();
    // this.signOut();
    console.log("home page constructor");
    // this.storage.get('user').then((user) => {
    //   console.log("home user emit -----");
    //   console.log(user);
    // });
    authService.emitChange.subscribe(text => {
      // if(typeof(text) === "boolean") {
        this.storage.get('user').then((user) => {
          console.log("home user emit -----");
          console.log(user);
          if (user != null && user != undefined) {
            this.user = JSON.parse(user);
            this.isLogin = true;
            console.log(this.isLogin);
          }
          else {
            this.isLogin = false;
            this.user = new User();
          }
        });
      // }
    });

  }
  ngOnInit() {
    this.getMenuCategories() ;
    this.getDataAll() ;
  }
  initializeApp() {
    this.platform.ready().then(() => {
      this.database = Database.getInstance();
      this.cart = Cart.getInstance();

      this.menuItems = this.database.parentCategory();
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });

    console.log(ENV);
    if (ENV.environment === 'dev') {
      console.log('development');
      // Run without the `--prod` flag.
      // Any keys defined in `dev.json` will be available on the `ENV` object.
    } else if (ENV.environment === 'prod') {
      console.log('production');
      // Run with the `--prod` flag.
      // Any keys defined in `prod.json` will be available on the `ENV` object.
    }
  }

  toggleItems(cat: Menu) {
    cat.parentShow = !cat.parentShow;
    this.menues.forEach(item => {
      if (item.id !== cat.id) {
        item.parentShow = false;
      }
    })
  }

  openPage(page) {
    // close the menu when clicking a link from the menu
    this.menu.close();
    this.nav.setRoot(page);
    this.storage.set('mahmoud','mahmoudi');
    //alert(page)

    // navigate to the new page if it is not the current page
    // if(page.name === 'SigninPage') {
    //   this.nav.setRoot(page.name);
    // }
    // } else {
    //   this.nav.setRoot(page.name, { tabIndex: page.index, tabName: page.tabName, detail: page.detail });
    // }
  }

  categories(category:Menu) {
    if( category.parent_id != 0 ) {
     // this.navCtrl.push('CategoriesPage', { Menu: category })
      this.nav.setRoot('TabsPage', { tabIndex: 0,page:'CategoriesPage', detail: category });
      this.menu.close();
    }

    //this.nav.setRoot('TabsPage', { tabIndex: 0, parent: menuItem, detail: child.name });
  }
  getDataAll() {
    this.api.get(this.homeDataUrl).then((data) => {
      this.data.goodCategories$ = data.goodCats;
      this.data.sliders$ = data.sliders;
      this.data.brands$ = data.brands;
      this.data.popularProducts$ = data.popularProducts;
      this.data.recentlyProducts$ = data.recentlyProducts;
      this.data.specailSellProducts$ = data.specailSellProducts;
      console.log("good cats")
      console.log(this.data.goodCategories$)
      this.data.emitBlog(true);
      // this.popup.open(   {title: 'My Title',
      //   message: 'My Message'}) ;
    });
  }

  getMenuCategories(){
    this.api.get(this.categoryMenuUrl).then((res)=>{
      this.secondMenues = res;
      console.log("menu")
      console.log(res)
      let len = res.length;
      console.log(res);
      this.secondMenues.forEach((menu,i) => {
        menu.parentShow = false ;
        this.api.getOne(this.categorySubMenuUrl,menu.id).then((subs)=>{
          console.log(subs);
          menu.sub_category = subs;
          if(subs.length > 0){
            menu.have_sub = true;
          }
          if(i == len-1){
            this.menues = this.secondMenues;
            console.log(this.menues);
          }
        });

      });

    });
  }

  signOut() {
    this.storage.get('user').then((user) => {
      console.log("signout");
      console.log("user");
      console.log(user);
      if(user){
        this.storage.remove('user');
        this.storage.remove('token');
        this.isLogin = false;
        this.user = new User();
        this.authService.emit(true);
      }
    });
  }

}
