
import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject } from "rxjs/Rx";

@Injectable()
export class Service {
  emitChangeCompare$: Subject<any> = new BehaviorSubject<any>(null);
  constructor() { }
  emitCompare(value: any) {
    this.emitChangeCompare$.next(value);
  }
  get emitChangeCompare(): BehaviorSubject<any> {
    return (this.emitChangeCompare$ as BehaviorSubject<any>);
  }
}
