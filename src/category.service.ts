import { Injectable }    from '@angular/core';
import { Headers, Http ,RequestOptions} from '@angular/http';

import 'rxjs/add/operator/toPromise';
import {Category1} from './category';
import {baseUrl} from './baseurl';
@Injectable()
export class CategoryService {
    private headers = new Headers({'Content-Type': 'application/json'});
    private option = new RequestOptions({headers:this.headers});

    private url=baseUrl+'api/categories';
    constructor(private http: Http) {
    }


    create(formData:FormData): Promise<any> {
         formData.append('token',sessionStorage.getItem('token'));
        return this.http
            .post(this.url, formData)
            .toPromise()
            .then(res => res.json() as Category1)
            .catch(this.handleError);
    }

    getCategories(): Promise<Category1[]> {
        const url = this.url;
        return this.http.get(url)
            .toPromise()
            .then(response => response.json() as Category1[])
            .catch(this.handleError);
    }


    delete(id: number): Promise<void> {
      const url = `${this.url}/${id}/${sessionStorage.getItem('token')}`;
      return this.http.delete(url, {headers: this.headers})
      .toPromise()
      .then((res) => res.json() )
      .catch(this.handleError);
    }
    update(name:string,parent_id:string,id:number): Promise<any> {
        const url = `${this.url}/${id}/${sessionStorage.getItem('token')}`;
        const body = JSON.stringify({name:name,parent_id:parent_id});
        const headers = new Headers ({'Content-Type':'application/json'});
        return this.http
            .put(url, body,{headers:headers})
            .toPromise()
            .then((res) => res.json() as Category1)
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }
}
//   const body = JSON.stringify({content:newContent});
//         const headers = new Headers ({'Content-Type':'application/json'});
//         return this.http.put('http://127.0.0.1:8000/api/quote/'+id,body,{headers:headers})
