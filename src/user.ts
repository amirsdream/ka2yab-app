export class User{
  id:number=0;
  firstName:string="";
  lastName:string="";
  email:string="";
  phone:string="";
  mobile:string="";
  pass:string="";
  role:string="";
  zip_code:string="";
  address:string="";
  active:string="";
  city: string = "";
  state: string = "";
  shopping_name: string = "";
  agent_id :number = 0;
  marketer_id: number = 0;
  seller_id:number = 0;
  marketers: User[] = [];
  btn: string  = '' ;
  activeDesc: string = '' ;
  name: string = '';

}
