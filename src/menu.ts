import {Product} from './product';
import {Category1} from './category';
export class Menu{
    id:number=0;
    name:string="";
    description:string="";
    parent_id:number=0;
    title:string="";
    numberOfProduct:number=0;
    products:Product[] =[];
    sub_category:Category1[] = [];
    have_sub : boolean = false ;
    parentShow: boolean = false ;
}
