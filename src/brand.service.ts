import { Injectable }    from '@angular/core';
import { Headers, Http ,RequestOptions} from '@angular/http';

import 'rxjs/Rx';
import {Observable} from "rxjs";
import 'rxjs/add/operator/toPromise';
// import {Category} from './category';

@Injectable()
export class BrandService {
    private headers = new Headers({'Content-Type': 'application/json'});
    private option = new RequestOptions({headers:this.headers});

    private url='http://127.0.0.1:8000/api/brands';
    constructor(private http: Http) {
    }


    create(url:string,formData:FormData): Promise<any> {
        return this.http
            .post(url, formData)
            .toPromise()
            .then(res => res.json() )
            .catch(this.handleError);
    }

    get(url:string): Promise<any> {
        // alert("salam");
        return this.http.get(url)
            .toPromise()
            .then(response => response.json() )
            .catch((error)=>{
                console.error('in error ', error)
                // console.log(Promise.reject(error.message || error))
                if(error.status == 0){
                    // return Promise.reject(error.message || error);
                    setTimeout(()=>{
                        // this.get(url)
                        // return Promise.reject(error.message || error);
                        this.get(url);
                        // return Promise.reject(error.message || error);
                        //console.log(Promise.reject(error.message || error))
                        // alert(error.status);
                    },200);
                    // alert("error 500");
                }
                else{
                     return Promise.reject(error.message || error);
                }

            });
    }

    getOne(url:string,id:number): Promise<any> {
        const urlGet = `${url}/${id}`;
        return this.http.get(urlGet)
            .toPromise()
            .then(response => response.json() )
            .catch((error)=>{
                console.error('error ', error)
                if(error.status == 0){
                    setTimeout(()=>{
                        // this.get(url);

                        this.getOne(url,id);
                        // alert(error.status);
                    },200);
                    // alert("error 500");
                }
                else{
                     return Promise.reject(error.message || error);
                }
            });
    }

  getOneWithSlug(url:string,slug:string): Promise<any> {
    const urlGet = `${url}/${slug}`;
    return this.http.get(urlGet)
      .toPromise()
      .then(response => response.json() )
      .catch(this.handleError);
  }
    delete(url:string,id: number): Promise<void> {
      const urlDelete = `${url}/${id}`;
      return this.http.delete(urlDelete, {headers: this.headers})
      .toPromise()
      .then((res) => res.json() )
      .catch(this.handleError);
    }
    update(url:string,formData:FormData): Promise<any> {
    // const url = `${this.url}/${id}`;
    // const body = JSON.stringify({name:name,parent_id:parent_id});
    const headers = new Headers ({'Content-Type':'application/json'});
    return this.http
      .put(url, formData,{headers:headers})
      .toPromise()
      .then((res) => res.json())
      .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('in error barabar ast ba ', error);
        // if()
         // for demo purposes only
        return Promise.reject(error.message || error);
    }

// -----------------------------------------------------------------------------------------------

  createObservable(url: string, formData: FormData) {
    return this.http.post(url, formData).map((response) => {
      return response.json();
    }).retry(10);

  }

  getObservable(url: string): Observable<any> {
    return this.http.get(url).map(( response ) => {
          return response.json();
        }
      ).retry(10);
  }
  getOneObservable(url: string, id: number): Observable<any> {

    const urlGet = `${url}/${id}`;
    return this.http.get(urlGet).map(( response ) => {
        return response.json();
      }
    ).retry(10);
  }
  getOneWithSlugObservable(url: string, slug: string): Observable<any> {
    const urlGet = `${url}/${slug}`;
    return this.http.get(urlGet).map(( response ) => {
        return response.json();
      }
    ).retry(10);
  }
  deleteObservable(url: string, id: number) {
    const urlDelete = `${url}/${id}`;
    return this.http.delete(urlDelete)
      .map(
        (response) => {
          return response.json();
        }).retry(10);
  }
  // updateQuote(id :number,newContent:string){
  //   const body = JSON.stringify({content:newContent});
  //   const headers = new Headers ({'Content-Type':'application/json'});
  //   return this.http.put('http://127.0.0.1:8000/api/quote/'+id,body,{headers:headers})
  //     .map(
  //       (response:Response)=>response.json()
  //     );
  // }
  //
  // deleteQuote(id :number){
  //   return this.http.delete('http://127.0.0.1:8000/api/quote/'+id);
  // }
    // private handleErrorGet(error: any,):Promise<any>{
    //     console.error('in error barabar ast ba ', error.status)
    //     if(error.status == 500){
    //         setTimeout(()=>{
    //             this
    //         },5000);
    //        // alert("error 500");
    //     }
    //     return Promise.reject(error.message || error);
    // }
}
//   const body = JSON.stringify({content:newContent});
//         const headers = new Headers ({'Content-Type':'application/json'});
//         return this.http.put('http://127.0.0.1:8000/api/quote/'+id,body,{headers:headers})
