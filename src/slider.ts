import { SliderCaptionImage } from "./slider_caption_image";
import { SliderCaptionTitle } from "./slider_caption_title";

export class Slider{
    id:number = 0;
    image_name:string = "";
    data_transition:string = "";
    data_masterspeed:string = "";
    images:SliderCaptionImage[] = [] ;
    titles:SliderCaptionTitle[] = [];
    
}