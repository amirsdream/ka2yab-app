import {Product} from './product';

export class Category1{
    id:number=0;
    name:string="";
    description:string="";
    parent_id:number;
    title:string="";
    numberOfProduct:number=0;
    products:Product[] =[];
    image_name:string = "";
    selected:boolean = false;
}
