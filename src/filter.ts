import { Filtervalue } from "./filtervalue";
export class Filter{
    id:number=0;
    name:string="";
    values:Filtervalue[] = [];
    selectedValue = new Filtervalue(); 
}