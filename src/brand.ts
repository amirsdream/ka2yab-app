import {Image} from './image';
import {Category1} from './category';
import {Attribute} from './attribute';

export class Brand{
    id:number = 0;
    name:string = "";
    user_id:number = 0;
    average_view:number = 0;
    image_name:string = "";
    selected: boolean = false;
}
