import {Categoryattribute} from "./categoryattribute";

export class Attribute{
    id:number=0;
    title:string="";
    description:string="";
    product_id:number = 0;
    categoryattribute_id:number = 0;
    categoryattribute = new Categoryattribute();
    selected: boolean = false;
}
