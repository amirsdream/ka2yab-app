
import {User} from './user';
export class Comment {

  id: number = 0;
  message: string = "";
  active: boolean = false;
  user = new User();

}
