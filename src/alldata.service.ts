import { Injectable } from '@angular/core';
import {BrandService} from './brand.service';
import {Blog} from './blog';
import {Product} from './product';
import {Slider} from './slider';
import {Category1} from './category';
import {Brand} from './brand';

import { Subject, BehaviorSubject } from "rxjs/Rx";

@Injectable()
export class AlldataService {
  emitChangeBlog$: Subject<any> = new BehaviorSubject<any>(null);
  constructor() { }
  emitBlog(value: any) {
    this.emitChangeBlog$.next(value);
  }
  get emitChangeBlog(): BehaviorSubject<any> {
    return (this.emitChangeBlog$ as BehaviorSubject<any>);
  }

  blogs$:Blog[] = [];
  recentlyProducts$:Product[] = [];
  sliders$:Slider[] =[];
  specailSellProducts$:Product[] = [];
  goodCategories$:Category1[] = [];
  popularProducts$:Product[] = [];
  brands$:Brand[] = [];



}
