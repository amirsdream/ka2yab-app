import {Attribute} from "./attribute";
export class Categoryattribute{
    id:number=0;
    name:string="";
    check:string="";
    category_id:number = 0;
    attributes:Attribute[] = [];
    full:boolean = true;
    selectedAttrs:string[] = [];
    selectedAttributes:Attribute[] = [];
}
