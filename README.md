# ionFull Ecommerce App 9ecom

[![Donate](https://www.paypalobjects.com/en_US/i/btn/btn_donate_LG.gif)](https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=quanganh%40aiti%2ecom%2evn&lc=VN&item_name=Ionic2%20Calendar&item_number=ionic2calendar&no_note=0&currency_code=USD&bn=PP%2dDonationsBF%3abtn_donateCC_LG%2egif%3aNonHostedGuest)

If you have any payment problem or cannot pay via **Paypal**, please pay via https://sellfy.com/p/THrc/ or contact me: **quanganh@aiti.com.vn**.

# 9ecom
9ecom is a Nice Full Ionic2 E-commerce themes, design for easy to customize and ready to use. It contains many useful elements like **Scrollable Tabs**, **Filter with Vertical Tabs**, **Expand/Collapse Menu** etc. You can easy to change color style and images to adapt with what you need. Easy to put the data to view with model. It's the best theme for your e-commerce or shopping app by ionic 2.

# About me
My name is Quang Anh. I'm a full stack developer (NodeJS + frontend).
View my apps at: https://market.ionic.io/user/74043

# Screens
1. Splash
2. Intro
3. Login
4. Register
5. Home
6. Hot Offers
7. Categories
8. Product
9. Filter
10. My Cart
11. Shipping Address
12. Payment with Promotion
13. Checkout
14. Thank Page
15. Search
16. Profile
17. My Orders
18. Wishlist
19. Saved Address

# Test before you buy
You can test the app using Ionic View (ID: **620d8406**) (Please note that when testing on Ionic View some Cordova plugins will not work) Download it here http://view.ionic.io/

# Support
User Guides: https://docs.google.com/document/d/1xcUMypwdzVwyEYzW4KpI9gprnZoU-QA0WjNjHtef0bU

If you need technical support or have any questions, please send me a message via: quanganh@aiti.com.vn

# Screenshots
![home](http://i.imgur.com/ELxlgIkm.jpg)
![hot-offers](http://i.imgur.com/jmcd1k5m.jpg)
![categories](http://i.imgur.com/9WMtSfHm.jpg)
![product](http://i.imgur.com/95H49osm.jpg)
![filters](http://i.imgur.com/WxO5Q8Gm.jpg)
![menus](http://i.imgur.com/sY4ZgW2m.jpg)
![my-cart](http://i.imgur.com/topFN0Gm.jpg)
![checkout](http://i.imgur.com/8hI7Sncm.jpg)
![payment](http://i.imgur.com/5EjmcTam.jpg)
![confirmation](http://i.imgur.com/BsljySTm.jpg)
![thank](http://i.imgur.com/m2cWwifm.jpg)
![search](http://i.imgur.com/xLYJUtmm.jpg)
![profile](http://i.imgur.com/BkS9pxMm.jpg)
![wishlist](http://i.imgur.com/YV38oVXm.jpg)

# Update 
- 26th Sep 2017: Update to Ionic 3.6.1
- 21th Jun 2017: Update to Ionic 3.4.2
- 15th May 2017: Update libs & fix Scrollable Tabs
- 25th Apr 2017: fix Android scroll
- 18th Apr 2017: Ionic v3 with Lazy Loading Pages and Components
- 14th Apr 2017: to Ionic v3

## Thanks Peerbits Solution for inspired design
https://www.behance.net/gallery/41369903/Free-UI-PSD-for-eCommerce-mobile-app

# Setup and Running 
- npm install -g ionic
- git clone https://github.com/quanganh206/nicecommerce
- npm install 
- npm build

# Environment 
```
Cordova CLI: 6.5.0 
Ionic Framework Version: 2.3.0
Ionic CLI Version: 2.2.1
Ionic App Lib Version: 2.2.0
Ionic App Scripts Version: 1.1.4
ios-deploy version: 1.9.0 
ios-sim version: 5.0.8 
OS: macOS Sierra
Node Version: v7.6.0
Xcode version: Xcode 8.2.1 Build version 8C1002
```